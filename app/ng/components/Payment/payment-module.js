/**
 * Created by yunusemre on 2.07.2016.
 */


angular.module('myApp.payment', ['ngResource', 'ui.router'])

    .config(function ($stateProvider, paymentdetailsState) {
        $stateProvider
            .state(paymentdetailsState.name, paymentdetailsState.options);
    });
