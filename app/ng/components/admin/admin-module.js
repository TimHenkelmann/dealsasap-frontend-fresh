/**
 * Created by yunusemre on 30.06.2016.
 */

angular.module('myApp.admin', ['ngResource', 'ui.router'])

    .config(function ($stateProvider, adminpanelState) {
        $stateProvider
            .state(adminpanelState.name, adminpanelState.options);
    });
