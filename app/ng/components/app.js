'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['ngFileUpload','ui.router', 'angularUtils.directives.dirPagination', 'myApp.buyingoffers', 'myApp.admin','myApp.home', 'myApp.user', 'myApp.payment', 'payment', 'myApp.offerresponse', 'myApp.myoffer',
                         'templates', 'ncy-angular-breadcrumb', 'ngMaterial', 'ngMessages', 'angular-notification-icons','ngAnimate'])

.config(function($stateProvider, $urlRouterProvider, $mdIconProvider, $resourceProvider, $httpProvider, $breadcrumbProvider, $mdThemingProvider) {

    // For any unmatched url, redirect to /home
    $urlRouterProvider
    .otherwise("/home");


    $stateProvider
    .state('root', {

        abstract: true,
        templateUrl: "views/root/root.html"
    })
    ;

    $mdIconProvider
    .iconSet('content', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg')
    .iconSet('action', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg')
    .iconSet('image', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg')
    .iconSet('editor', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-editor.svg')
    .iconSet('navigation', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg');

    //this overrides the defaults actiosn for all $resources
    angular.extend($resourceProvider.defaults.actions, {

        update: {
            method: "PUT"
        }

    });

    $httpProvider.interceptors.push('reqErrInterceptor');
    //auth interceptor
    $httpProvider.interceptors.push('authInterceptor');

    $breadcrumbProvider.setOptions({
        templateUrl:"components/breadcrumbs/breadcrumbs.html",
    });

    $mdThemingProvider.definePalette('mainPalette', {
        '50': '#f4fcfd',
        '100': '#b2e8f2',
        '200': '#82d9ea',
        '300': '#45c7e0',
        '400': '#2bbfdc',
        '500': '#21acc7',
        '600': '#1d95ad',
        '700': '#187f93',
        '800': '#146878',
        '900': '#10515e',
        'A100': '#f4fcfd',
        'A200': '#b2e8f2',
        'A400': '#2bbfdc',
        'A700': '#187f93',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': '50 100 200 300 400 500 A100 A200 A400'
    });

    $mdThemingProvider.theme('default').primaryPalette('mainPalette');
});
