/**
 * Created by TimH on 02-Jul-16.
 */

'use strict';

angular.module('myApp.buyingoffers')

    .directive('mvBuyingofferDetail', function() {
        return {
            restrict: 'A',
            scope: {
                buyingoffer: '=',
                sameuser: '=',
                createEditOfferDialog: '=',
                deleteBuyingoffer:'=',
                createOfferresponseDialog:'=',
                processBuyingoffer:'=',
                loggedIn:'='
            },
            templateUrl: 'components/buyingoffer-detail/buyingoffer-detail.html'
        };
    });

