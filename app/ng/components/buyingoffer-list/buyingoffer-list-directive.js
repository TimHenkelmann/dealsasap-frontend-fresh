/**
 * Created by Anupama on 08-06-2016.
 */

'use strict';

angular.module('myApp.buyingoffers')

    .directive('mvBuyingofferList', function() {
        return {
            restrict: 'A',
            scope: {
                buyingoffer: '='
            },
            templateUrl: 'components/buyingoffer-list/buyingoffer-list.html'
        };
    });

