'use strict';

angular.module('myApp.buyingoffers')

    .factory('Buyingoffer', function($resource) {
        return $resource('http://localhost:3000/api/buyingoffers/:buyingofferId', {buyingofferId: '@_id'});

    });