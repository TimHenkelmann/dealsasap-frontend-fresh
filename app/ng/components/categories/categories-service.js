'use strict';

angular.module('myApp.buyingoffers')

    .factory('Category', function($resource) {
        return $resource('http://localhost:3000/api/categories/parent/:parentId', {parentId: '@parent'});

    });