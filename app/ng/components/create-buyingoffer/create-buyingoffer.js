angular.module('myApp.buyingoffers')
    .controller('CreateBuyingofferCtrl', function($scope, Buyingoffer, Category, $mdDialog,
                                                  $rootScope, currUser, $http, $filter) {

        $scope.buyingoffer = new Buyingoffer();
        $scope.conditions = [{
            abbr: "New (original packaging)",
            short_desc: "Item original and new",
            long_desc: ""
        },{
            abbr: "New (packaging opened or missing)",
            short_desc: "Item new but not original",
            long_desc: ""
        },
            {
                abbr: "Barely used",
                short_desc: "Item barely used",
                long_desc: ""
            },
            {
                abbr: "Quite used",
                short_desc: "Item quite used",
                long_desc: ""
            },
            {
                abbr: "Damaged/broken",
                short_desc: "Item damaged/broken",
                long_desc: ""
            }];
        $scope.price = 0;

        // get user's addresses
        $http.get('http://localhost:3000/api/address')
        .success(function(response) {
            $scope.addresses = response;
        })
        .error(function(response) {
            $scope.error = response.message;
        });

        $scope.pickAddress = function(address) {
            $scope.buyingoffer.shippingAddress = address;
        };

        $scope.validationdate=new Date();
        $scope.minDate = new Date();

        $scope.save = function() {
            
                    $scope.buyingoffer.user = currUser.getUser()._id;
                    $scope.buyingoffer.$save()
                        .then(function(){
                            $rootScope.$broadcast('buyingofferCreated', $scope.buyingoffer);
                            $mdDialog.hide(true);
                        }).catch(function(){
                        $mdDialog.hide(false);
                    });        
            
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        ///////////////////////////

        Category.query({parentId:'All Categories'}).$promise.then(function(categories){

            $scope.categories = $filter('filter')(categories, function (value, index) {
                return value._id !== 'All Categories';
            });
        })
        ;

        $scope.getChildren = function(cid){
            return Category.query({parentId:cid});
        };
        

    });
