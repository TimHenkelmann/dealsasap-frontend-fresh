/**
 * Created by TimH on 22-Jun-16.
 */
angular.module('myApp.buyingoffers')
    .controller('CreateOfferresponseCtrl', function ($scope, Offerresponse, Upload, $mdDialog, $rootScope, currUser) {


        $scope.offerresponse = new Offerresponse();
        $scope.buyingoffer = $rootScope.buyingoffer;
        

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.upload = function () {
            
            $scope.currUser = currUser.getUser()._id;
            Upload.upload({
                url: 'http://localhost:3000/api/offerresponse',
                data: {file: $scope.offerresponse.pictures, 'message': $scope.offerresponse.message, 'sender': $scope.currUser, 'date': Date.now(),'buyingoffer':$scope.buyingoffer._id, 'addressee':$scope.buyingoffer.user}
            }).then(function (resp) {
                $mdDialog.hide(true);
            }).catch( function (resp) {
                console.log('Error status: ' + resp.status);
                $mdDialog.hide(false);
            });
        };


    });

