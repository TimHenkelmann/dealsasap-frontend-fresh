angular.module('myApp.home', ['ngResource', 'ui.router', 'myApp.buyingoffers'])

    .config(function ($stateProvider, homeState) {
        $stateProvider
            .state(homeState.name, homeState.options);
    })
    .controller('homeController',
        function ($scope, currUser, $mdDialog, $mdMedia, $mdToast, $rootScope, $http) {
            $scope.checkUser = function () {
                if (!currUser.loggedIn()) {
                    // user not logged in yet
                    alert('Please log in first');
                }
                else {
                    //user already logged in
                    alert('User logged in');
                }
            };

            $scope.showAlertLogin = function (ev) {
                // Appending dialog to document.body to cover sidenav in docs app
                // Modal dialogs should fully cover application
                // to prevent interaction outside of dialog
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Login required')
                        .textContent('Please login first or register an account')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(ev)
                );
            };

            $scope.createOffer = function (ev) {
                if (!currUser.loggedIn()) {
                    $scope.showAlertLogin();
                }
                else {
                    $rootScope.invalidateConnectionErrors = true;
                    $http.get('http://localhost:3000/api/payment/user/' + currUser.getUser()._id)
                        .success(function (response) {
                            if(!response.length){
                                $scope.showDialogCreatePayment();
                                $rootScope.invalidateConnectionErrors = false;
                            }
                            else{

                                $rootScope.invalidateConnectionErrors = false;
                                createBuyingofferDialog(ev);
                            }
                        })
                        
                }
            };

            function createBuyingofferDialog(ev) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    controller: "CreateBuyingofferCtrl",
                    templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    preserveScope: true
                })
                    .then(function (answer) {

                        if (answer) {
                            showSimpleToast('Request saved successfully');
                        } else {
                            showSimpleToast('An Error occured!');
                        }
                    }, function () {
                        showSimpleToast('Request creation cancelled');
                    });
            }
            
            $scope.showDialogCreatePayment = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Payment details missing')
                        .textContent('Unfortunately we could not find any payment details. Please make sure to add them to your profile before creating an offer')
                        .ariaLabel('Payment Details Missing Dialog')
                        .ok('OK')
                        .targetEvent(ev)
                )
            };
            function showSimpleToast(txt) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(txt)
                        .position('bottom right')
                        .hideDelay(3000)
                );
            }
        }
    );
