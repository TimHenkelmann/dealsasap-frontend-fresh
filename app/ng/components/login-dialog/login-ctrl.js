angular.module('myApp')
    .controller("login", function ($scope, $rootScope,currUser, $mdDialog) {
        $scope.username = '';
        $scope.pwd = '';
        $scope.errorText = '';

        $scope.login = login;
        $scope.cancel = cancel;

        function login() {
            currUser.login($scope.username, $scope.password).then(function () {
                $rootScope.$broadcast('userLoggedIn', currUser.getUser());
                $mdDialog.hide();
            }, function (response) {
                alert(response.status);
                if (response.status == "400" || response.status == "401") {
                    $scope.errorText = "Wrong username or password.";
                } else {
                    $scope.errorText = "An unknown error occured. please try again later.";
                }
            });
        }

        function cancel() {
            $mdDialog.cancel();
        }
    });
