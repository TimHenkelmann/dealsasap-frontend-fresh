angular.module('myApp.myoffer', ['ngResource', 'ui.router'])

.config(function($stateProvider, myOfferState) {
    $stateProvider
    .state(myOfferState.name, myOfferState.options);
})

