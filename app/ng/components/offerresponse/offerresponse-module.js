/**
 * Created by yunusemre on 27.06.2016.
 */

angular.module('myApp.offerresponse', ['ngResource', 'ui.router'])

    .config(function ($stateProvider, transactionsState) {
        $stateProvider
            .state(transactionsState.name, transactionsState.options);
    });
