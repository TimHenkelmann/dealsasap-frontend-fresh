/**
 * Created by TimH on 22-Jun-16.
 */
'use strict';

angular.module('myApp.offerresponse')

    .factory('Offerresponse', function($resource) {
        return $resource('http://localhost:3000/api/offerresponse/:offerresponseId', {offerresponseId: '@_id'});

    });
