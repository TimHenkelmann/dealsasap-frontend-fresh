/**
 * Created by yunusemre on 30.06.2016.
 */


'use strict';

angular.module('myApp.admin')

    .factory('Refundclaim', function($resource) {
        return $resource('http://localhost:3000/api/refundclaim/:refundclaimId', {refundclaimId: '@_id'});

    });

