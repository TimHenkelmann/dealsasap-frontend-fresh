angular.module('myApp')
    .controller("register", function ($scope, currUser, $mdDialog, $rootScope) {
        $scope.username = '';
        $scope.pwd = '';
        $scope.pwdConfirm
        $scope.errorText = '';

        $scope.register = register;
        $scope.cancel = cancel;

        function register() {
            currUser.register($scope.username, $scope.pwd, $scope.firstName, $scope.lastName, $scope.email)
            .then(function () {
                $mdDialog.hide();
                $rootScope.$broadcast('userLoggedIn', currUser.getUser());

            }, function (response) {
                debugger;
                if (response.status == 400 || response.status == 401) {
                    $scope.errorText = "An unknown error occured. please try again later.";
                }
            });
        }

        function cancel() {
            $mdDialog.cancel();
        }
    });
