angular.module('myApp')
    .directive('mvToolbar', function () {
        return {
            restrict: "A",
            templateUrl: "components/toolbar/toolbar.html",
            controller: function ($scope, currUser, $mdDialog, $mdMedia, $mdToast, $state, $http, $rootScope) {

                $scope.user = null;
                $scope.createBuyingofferDialog = createBuyingofferDialog;
                $scope.$on('userLoggedIn', function (ev, user) {
                    $scope.user = user;
                });

                $scope.showLoginDialog = showLoginDialog;
                $scope.showSignupDialog = showSignupDialog;
                $scope.logout = logout;

                $scope.$watch(function () {
                    return currUser.loggedIn();
                }, function (loggedIn) {
                    $scope.loggedIn = loggedIn;
                    if (loggedIn && !$scope.user) {
                        $scope.user = currUser.getUser();
                    }
                });


                $scope.showAlertLogin = function (ev) {
                    // Appending dialog to document.body to cover sidenav in docs app
                    // Modal dialogs should fully cover application
                    // to prevent interaction outside of dialog
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Login required')
                            .textContent('Please login first or register yourself')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('OK')
                            .targetEvent(ev)
                    );
                };
                /////////////////////

                function showLoginDialog() {
                    var useFullScreen = $mdMedia('xs');
                    $mdDialog.show({
                        controller: 'login',
                        templateUrl: 'components/login-dialog/login-dialog.html',
                        clickOutsideToClose: true,
                        fullscreen: useFullScreen
                    });
                };
                function showSignupDialog() {
                    var useFullScreen = $mdMedia('xs');
                    $mdDialog.show({
                        controller: 'register',
                        templateUrl: 'components/register-dialog/register-dialog.html',
                        clickOutsideToClose: true,
                        fullscreen: useFullScreen
                    });
                };

                function logout() {
                    currUser.logout();
                    $state.go('home');      // redirect to homepage when log out
                }

                function showSimpleToast(txt) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(txt)
                            .position('bottom right')
                            .hideDelay(3000)
                    );
                }

                function createBuyingofferDialog(ev) {
                    $rootScope.invalidateConnectionErrors = true;
                    $http.get('http://localhost:3000/api/payment/user/' + currUser.getUser()._id)
                        .success(function (response) {
                                if (!response.length) {
                                    $scope.showDialogCreatePayment();
                                    $rootScope.invalidateConnectionErrors = false;
                                }
                                else {


                                    $rootScope.invalidateConnectionErrors = false;
                                    if (!currUser.loggedIn()) {      // user not logged in
                                        showAlertLogin();
                                    }
                                    else {      // user already logged in

                                        var useFullScreen = ( $mdMedia('xs'));
                                        $mdDialog.show({
                                            controller: "CreateBuyingofferCtrl",
                                            templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                                            targetEvent: ev,
                                            clickOutsideToClose: true,
                                            fullscreen: useFullScreen,
                                            preserveScope: true
                                        })
                                            .then(function (answer) {

                                                if (answer) {
                                                    showSimpleToast('Request saved successfully');
                                                } else {
                                                    showSimpleToast('An Error occured!');
                                                }
                                            }, function () {
                                                showSimpleToast('Request creation cancelled');
                                            });
                                    }
                                }

                            }
                        )


                };
                $scope.showDialogCreatePayment = function (ev) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Payment details missing')
                            .textContent('Unfortunately we could not find any payment details. Please make sure to add them to your profile before creating an offer')
                            .ariaLabel('Payment Details Missing Dialog')
                            .ok('OK')
                            .targetEvent(ev)
                    )
                };

            }
        }
    });
