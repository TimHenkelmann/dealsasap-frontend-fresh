angular.module('myApp.user', ['ngResource', 'ui.router'])

.config(function ($stateProvider, profileState) {
    $stateProvider
    .state(profileState.name, profileState.options);
});

