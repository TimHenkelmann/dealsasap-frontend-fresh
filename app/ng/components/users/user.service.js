'use strict';

angular.module('myApp.user')

.factory('User', function($resource) {
    return $resource('http://localhost:3000/users/profile');

});
