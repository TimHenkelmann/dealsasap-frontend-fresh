/**
* Created by yunusemre on 30.06.2016.
*/


'use strict';

angular.module('myApp.admin')
.constant('adminpanelState', {
    name: 'adminpanel',
    options: {
        parent: 'root',
        url: '/adminpanel',

        views: {
            'content@root': {
                templateUrl: 'views/adminpanel/adminpanel.html',
                controller: 'adminpanelController'
            }
        },

        ncyBreadcrumb: {
            label: "adminpanel"
        }
    }
})
.controller('adminpanelController',
function($scope, $http, currUser, Offerresponse, Refundclaim, Buyingoffer, Upload, $resource, $mdDialog, $rootScope, $mdMedia, $mdToast) {


    $scope.claims = Refundclaim.query();
    $scope.initialize=function() {
        angular.forEach($scope.claims, function (claim) {
            $http.get('http://localhost:3000/users/getName/' + claim.seller)
            .success(function (response) {
                claim.seller = response;
            })
        });
        angular.forEach($scope.claims, function (claim) {
            $http.get('http://localhost:3000/users/getName/' + claim.buyer)
            .success(function (response) {
                claim.buyer = response;
            })
        });
    }

    $scope.test = "asasdadsd";

    $scope.getTransaction = function (tid) {
        return Offerresponse.get({offerresponseId: tid});
    };
    $scope.getBuyingoffer = function (bid) {
        return Buyingoffer.get({buyingofferId: bid});
    };

    $scope.resolve = function (claim) {
        var updatedclaim = claim;
        updatedclaim.isResolved = true;
        $http.put('http://localhost:3000/api/refundclaim/' + claim._id, {isResolved: true})
        .success(function (response) {
            showSimpleToast("Resolved succesfully");
        })
        .error(function (response) {
            showSimpleToast("error. please try again later");
        });
    };

    $scope.getUserName = function (uid) {
        $http.get('http://localhost:3000/users/getName/' + uid).success(function (data) {
            $scope.test=data.username;
        });
    }

});
