'use strict';

angular.module('myApp.buyingoffers')

    .constant('buyingofferDetailsState', {
        name: 'buyingoffers.detail',
        options: {
            url: '/{buyingofferId}',

            views: {
                "content@root": {
                    templateUrl: 'views/detail/buyingoffer-detail.html',
                    controller: 'BuyingofferDetailCtrl'
                }
            },

            resolve: {
                //we abuse the resolve feature for eventual redirection
                redirect: function ($state, $stateParams, Buyingoffer, $timeout, $q) {
                    var mid = $stateParams.buyingofferId;
                    var mid = $stateParams.buyingofferId;
                    if (!mid) {
                        //timeout because the transition cannot happen from here
                        $timeout(function () {
                            $state.go("buyingoffers.list");
                        });
                        return $q.reject();
                    }
                }
            },
            ncyBreadcrumb: {
                label: "{{$$childHead.$$childHead.buyingoffer.title}}",
                parent: "buyingoffers.list"
            }

        }
    })
    .controller('BuyingofferDetailCtrl', function ($scope, Buyingoffer, Offerresponse, Category, $mdToast, $rootScope,$mdDialog, $mdMedia, $stateParams, $state, currUser, $http, $filter) {

        $scope.buyingoffer = Buyingoffer.get({buyingofferId: $stateParams.buyingofferId}, function(buyingoffer) {
            $scope.buyingoffer.validationdate = new Date($scope.buyingoffer.validationdate);
        });
        Category.query({parentId:'All Categories'}).$promise.then(function(categories){

            $scope.categories = $filter('filter')(categories, function (value, index) {
                return value._id !== 'All Categories';
            });
        })
        ;
        //$scope.categories = Category.query({parentId:'All Categories'});
        $scope.getChildren = function(cid){
            return Category.query({parentId:cid});
        };
        $scope.sameuser;
        $scope.mayDelete = false;
        $scope.mayEdit = false;
        $scope.loggedIn = false;
        $scope.deleteBuyingoffer = deleteBuyingoffer;
        $scope.processBuyingoffer = processBuyingoffer;
        $scope.updateBuyingoffer = updateBuyingoffer;
        $scope.createOfferresponseDialog = createOfferresponseDialog;
        $scope.createEditOfferDialog = createEditOfferDialog;
        $scope.authed = false;
        $scope.requirePicture = false;

        $scope.cancelEditingBuyingoffer = function () {
            showSimpleToast("Editing cancelled");
        }

        $scope.$on('buyingofferUpdated', function(ev, buyingoffer){
            $scope.$evalAsync(function () {
                $scope.buyingoffer = buyingoffer;
            });
        });
        $scope.buyingoffer.$promise.then(function () {
            //$scope.mayDelete = $scope.buyingoffer.user && $scope.buyingoffer.user == currUser.getUser()._id;
            $scope.sameuser = $scope.buyingoffer.user && $scope.buyingoffer.user == currUser.getUser()._id;
        });

        $scope.$watch(function () {
            return currUser.loggedIn();
        }, function (loggedIn) {
            if (!loggedIn) {
                $scope.mayDelete = false;
                $scope.mayEdit = false;
                $scope.loggedIn = false;
            } else {
                $http.get('http://localhost:3000/api/address')
                    .success(function(response) {
                        $scope.addresses = response;
                    })
                    .error(function(response) {
                        $scope.error = response.message;
                    });
                $scope.loggedIn = true;
                $scope.mayEdit = $scope.buyingoffer.user == currUser.getUser()._id;
                $scope.mayDelete = $scope.buyingoffer.user == currUser.getUser()._id;
            }
        })

        // For offer editing section
        $scope.conditions = [{
            abbr: "New (original packaging)",
            short_desc: "Item original and new",
            long_desc: ""
        },{
            abbr: "New (packaging opened or missing)",
            short_desc: "Item new but not original",
            long_desc: ""
        },
        {
            abbr: "Barely used",
            short_desc: "Item barely used",
            long_desc: ""
        },
        {
            abbr: "Quite used",
            short_desc: "Item quite used",
            long_desc: ""
        },
        {
            abbr: "Damaged/broken",
            short_desc: "Item damaged/broken",
            long_desc: ""
        }];

        

        $scope.pickAddress = function(address) {
            $scope.buyingoffer.shippingAddress = address;
        };

        $scope.validationdate = new Date();
        $scope.minDate = new Date();

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.save = function() {
            $scope.buyingoffer.user = currUser.getUser()._id;
            $scope.updateBuyingoffer();
        };

        ////////////////////
        function updateBuyingoffer() {
            $http.put('http://localhost:3000/api/buyingoffers/' + $stateParams.buyingofferId, $scope.buyingoffer)
                .success(function(response) {
                    $scope.buyingoffer = response;      // store updated value
                    $scope.buyingoffer.validationdate = new Date($scope.buyingoffer.validationdate);
                    $rootScope.$broadcast('buyingofferUpdated', $scope.buyingoffer);
                    $mdDialog.hide(true);
                })
                .error(function(response) {
                    $mdDialog.hide(false);
                    showSimpleToast("error. please try again later");
                });
        }

        function deleteBuyingoffer(ev) {

            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to delete this request?')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Abort');

            var toastText;
            $mdDialog.show(confirm).then(function () {
                return $scope.buyingoffer.$remove().then(function () {
                    return $state.go('buyingoffers.list');
                }).then(function () {
                    showSimpleToast('Request deleted successfully');
                }, function () {
                    showSimpleToast("Error. Try again later");
                });
            }, function () {
                showSimpleToast("Delete aborted");
            })
        }

        function processBuyingoffer(ev) {

            var confirm = $mdDialog.confirm()
                .title('Do you really want to sell this item?')
                .textContent('Please notice that you have the item as described in the request and that it is ready for shipping!')
                .targetEvent(ev)
                .ok('Yes, I want to sell this item')
                .cancel('Abort');

            var toastText;
            $mdDialog.show(confirm).then(function () {
                var offerresponse= new Offerresponse();
                offerresponse.buyingoffer=$scope.buyingoffer;
                offerresponse.sender=currUser.getUser()._id;
                offerresponse.addressee=$scope.buyingoffer.user;
                offerresponse.isTransaction=true;
                offerresponse.date=Date.now();

                offerresponse.$save()
                    .then(function(){
                        $mdDialog.hide(true);
                        showSimpleToast("Sold successfully. Please see the shipping address in previous transactions");
                    }).catch(function(){
                    $mdDialog.hide(false);
                });

                $http.put('http://localhost:3000/api/buyingoffers/' + $scope.buyingoffer._id, {isSold: true})
                    .success(function (response) {
                    })
                    .error(function (response) {
                        showSimpleToast("error. please try again later");
                    });


            }, function () {
                showSimpleToast("Forward aborted");
            })
        }

        function createOfferresponseDialog(ev) {
            $rootScope.buyingoffer = $scope.buyingoffer;
            var useFullScreen = ( $mdMedia('xs'));
            var toastText;
            $mdDialog.show({
                controller: "CreateOfferresponseCtrl",
                templateUrl: 'components/create-offerresponse/create-offerresponse.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                fullscreen: useFullScreen,
                clickOutsideToClose: true
            })
                .then(function () {
                    showSimpleToast("Pictures were sent. You can check the status of your offer in \"My Requests/Offer\"");
                }, function () {
                    showSimpleToast("Forward aborted");
                })


        }


        function showSimpleToast(txt) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }

        function createEditOfferDialog(ev, transaction) {
            var useFullScreen = ( $mdMedia('xs'));
            $mdDialog.show({
                controller: "BuyingofferDetailCtrl",
                templateUrl: 'components/create-buyingoffer/edit-buyingoffer.html',
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                preserveScope:true
            })
            .then(function(answer) {

                if (answer) {
                    showSimpleToast('Request updated successfully');
                } else {
                    showSimpleToast('An Error occured!');
                }
            }, function() {
                showSimpleToast('Edit cancelled');
            });
        }

    });
