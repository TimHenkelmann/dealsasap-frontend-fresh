'use strict';

angular.module('myApp.home')
.constant('homeState', {
    name: 'home',
    options: {
        parent: 'root',
        url: '/home',

        views: {
            'content@root': {
                templateUrl: 'views/home/home.html',
                controller: 'homeController'
            }
        },

        ncyBreadcrumb: {
            label: "Home"
        }
    }
})
