'use strict';

angular.module('myApp.buyingoffers')

    .constant('buyingofferListState', {
        name: 'buyingoffers.list',
        options: {

            // Using an empty url means that this child state will become active
            // when its parent's url is navigated to. Urls of child states are
            // automatically appended to the urls of their parent. So this state's
            // url is '/buyingoffers' (because '/buyingoffers' + '').
            url: '',

            views: {
                'content@root': {
                    templateUrl: 'views/list/buyingoffer-list.html',
                    controller: 'BuyingofferListCtrl',
                },
                'outside@root': {
                    templateUrl: 'views/list/buyingoffer-list-buttons.html',
                    controller: 'buyingofferListButtonCtrl'
                }
            },

            ncyBreadcrumb: {
                label: "Browse"
            }

        }

    })

    .controller('BuyingofferListCtrl', function ($scope, Buyingoffer, Category, $http, $resource) {

        $scope.buyingoffers = Buyingoffer.query();

        this.category = "home";

        Category.query({parentId: 'All Categories'}).$promise.then(function (categories) {
            $scope.categories = categories;
            $scope.categories[0].makePrimary = 'md-primary';
        });

        $scope.getChildren = function (cid) {
            return Category.query({parentId: cid._id});
        };

        $scope.updateData = function (catgry) {

            $http.get('http://localhost:3000/api/buyingoffers/category/' + catgry._id).success(function (data) {
                $scope.buyingoffers = data;
                angular.forEach($scope.categories, function (category) {
                    category.makePrimary = '';
                    if (category.children) {
                        angular.forEach(category.children, function (category) {
                            category.makePrimary = '';
                            if (category.grandchildren) {
                                angular.forEach(category.grandchildren, function (category) {
                                    category.makePrimary = '';
                                });
                            }
                        });
                    }
                });


                catgry.makePrimary = 'md-primary';

            });

        };

        $scope.$on('buyingofferCreated', function (ev, buyingoffer) {
            $scope.buyingoffers.push(buyingoffer);
        });


    })

    .controller('buyingofferListButtonCtrl', function ($scope, $mdMedia, $mdDialog, $mdToast, currUser, $rootScope, $http) {

        $scope.createBuyingofferDialog = createBuyingofferDialog;
        $scope.authed = false;

        $scope.$watch(function () {
            return currUser.loggedIn();
        }, function (loggedIn) {
            $scope.authed = loggedIn;
        });

        ////////////////////////////////////

        function createBuyingofferDialog(ev) {
            $rootScope.invalidateConnectionErrors = true;
            $http.get('http://localhost:3000/api/payment/user/' + currUser.getUser()._id)
                .success(function (response) {
                    if (!response.length) {
                        $scope.showDialogCreatePayment();
                        $rootScope.invalidateConnectionErrors = false;
                    }
                    else {
                        $rootScope.invalidateConnectionErrors = false;
                        var useFullScreen = ( $mdMedia('xs'));
                        $mdDialog.show({
                            controller: "CreateBuyingofferCtrl",
                            templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                            targetEvent: ev,
                            clickOutsideToClose: true,
                            fullscreen: useFullScreen,
                            preserveScope: true
                        })
                            .then(function (answer) {

                                if (answer) {
                                    showSimpleToast('Request saved successfully');
                                } else {
                                    showSimpleToast('An Error occured!');
                                }
                            }, function () {
                                showSimpleToast('Request creation cancelled');
                            });
                    }
                })
        };
        $scope.showDialogCreatePayment = function (ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Payment details missing')
                    .textContent('Unfortunately we could not find any payment details. Please make sure to add them to your profile before creating an offer')
                    .ariaLabel('Payment Details Missing Dialog')
                    .ok('OK')
                    .targetEvent(ev)
            )
        };

        function showSimpleToast(txt) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }
    });
