'use strict';


angular.module('myApp.myoffer')
    .constant('myOfferState', {
        name: 'buyingoffers.myoffer',
        options: {
            parent: 'root',
            url: '/myoffer',
            views: {
                'content@root': {
                    templateUrl: 'views/myoffer/myoffer.html',
                    controller: 'MyOfferController'
                }
            },

            ncyBreadcrumb: {
                label: "My Requests/Offers"
            }
        }
    })
    .controller('MyOfferController',
        function ($scope, $http, currUser, $rootScope, $mdToast, $mdDialog,
                  $mdMedia, Buyingoffer, Offerresponse, Category, $stateParams, $state, $filter) {


            $scope.OfferSelected = false;
            $scope.mayDelete = true;    // always true because it's in that user's page
            $scope.mayEdit = true;
            $scope.deleteBuyingoffer = deleteBuyingoffer;
            $scope.updateBuyingoffer = updateBuyingoffer;
            $scope.createEditOfferDialog = createEditOfferDialog;
            $scope.createOfferresponseDialog = createOfferresponseDialog;
            $scope.authed = false;


            //---------------------------HTTP CALLS AND $RESOURCE CALLS---------------------------
            // Get all buyingoffers from this user
            $http.get('http://localhost:3000/api/buyingoffers/user/' + currUser.getUser()._id)
                .success(function (response) {
                    $scope.buyingoffers = $filter('filter')(response, {isSold: false});
                    angular.forEach($scope.buyingoffers, function (buyingoffer) {
                        $http.get('http://localhost:3000/api/offerresponse/buyingoffer/' + buyingoffer._id)
                            .success(function (response) {
                                if (response == '') {
                                    buyingoffer.haveNewResponse = '';
                                    buyingoffer.hasResponsesClass = '';
                                }
                                else {
                                    buyingoffer.haveNewResponse = '';
                                    buyingoffer.hasResponsesClass = '';
                                    var keepgoing = true;
                                    angular.forEach(response, function (offerresponse) {
                                        if (keepgoing) {
                                            if (offerresponse.isResponded == false) {
                                                buyingoffer.haveNewResponse = '(New Offer!)';
                                                buyingoffer.hasResponsesClass = 'md-primary';
                                                keepgoing = false;
                                            }
                                        }
                                    });
                                }
                            });
                    });
                })
                .error(function (response) {
                    alert('error');
                    $scope.error = response.message;
                });

            // Get all offerresponses of an item that this user request
            $http.get('http://localhost:3000/api/offerresponse/Addressee/' + currUser.getUser()._id)
                .success(function (data) {
                    $rootScope.offerresponses = data;
                    angular.forEach($rootScope.offerresponses, function (offerresponse) {

                        $http.get('http://localhost:3000/users/getRating/' + offerresponse.sender)
                            .success(function (response) {
                                offerresponse.rating = response;
                            });

                        $http.get('http://localhost:3000/users/getName/' + offerresponse.sender)
                            .success(function (response) {
                                offerresponse.sender = response;
                            });
                    });
                })
                .error(function (response) {
                    alert('error');
                    $scope.error = response.message;
                });

            // Get all offerresponses which this user sent
            $http.get('http://localhost:3000/api/offerresponse/Sender/' + currUser.getUser()._id)
                .success(function (data) {
                    $scope.senderresponses = data;
                    angular.forEach($scope.senderresponses, function (senderresponse) {
                        $http.get('http://localhost:3000/api/buyingoffers/' + senderresponse.buyingoffer)
                            .success(function (response) {
                                senderresponse.buyingoffer = response;
                            });
                    });
                })
                .error(function (response) {
                    alert('error');
                    $scope.error = response.message;
                });


            //---------------------------$on---------------------------
            $scope.$on('buyingofferCreated', function (ev, buyingoffer) {
                $scope.buyingoffers.push(buyingoffer);
            });
            $scope.$on('buyingofferDeleted', function (ev, buyingoffer) {
                $scope.buyingoffers = $filter('filter')($scope.buyingoffers, function (value, index) {
                    return value._id !== buyingoffer._id;
                });
            });
            $scope.$on('offerresponseDeclined', function (ev, offerresponse) {
                $rootScope.offerresponses = $filter('filter')($rootScope.offerresponses, function (value, index) {
                    return value._id !== offerresponse._id;
                });
                $rootScope.buyingoffer.haveNewResponse = '';
                $rootScope.buyingoffer.hasResponsesClass = '';
            });
            $scope.$on('buyingofferIsSold', function (ev, offerresponse) {
                $scope.buyingoffers = $filter('filter')($scope.buyingoffers, function (value, index) {
                    return value._id !== offerresponse.buyingoffer;
                });
                $scope.OfferSelected = false;
            });
            $scope.$on('buyingofferUpdated', function (ev, buyingoffer) {
                $scope.$evalAsync(function () {
                    $rootScope.buyingoffer = buyingoffer;
                });
            });


            //---------------------------functions---------------------------
            $scope.setBuyingoffer = function (selectedbuyingoffer) {
                $rootScope.buyingoffer = selectedbuyingoffer;
                $rootScope.sameuser = (selectedbuyingoffer.user === currUser.getUser()._id);
                $scope.OfferSelected = true;
                $scope.ResponseSelected = false;
            };
            $scope.setOfferresponse = function (selectedresponse) {
                $rootScope.buyingoffer = selectedresponse.buyingoffer;
                $rootScope.sameuser = (selectedresponse.buyingoffer.user === currUser.getUser()._id);
                $scope.OfferSelected = true;
                $scope.ResponseSelected = true;
                if (!selectedresponse.isResponded) {
                    $scope.status = 'Pending';
                    $scope.statusColor = 'orange';
                }
                else if (!selectedresponse.isTransaction) {
                    $scope.status = 'Declined';
                    $scope.statusColor = 'red';
                }
                else {
                    $scope.status = 'Accepted, please go to "previous transactions" in your profile to see more details';
                    $scope.statusColor = 'green';
                }
            };
            $scope.hasResponses = function (currentBuyingoffer) {
                for (var offerresponse in $rootScope.offerresponses) {
                    if (offerresponse.buyingoffer === currentBuyingoffer._id) {
                        return true;
                    }
                }
            };
            $scope.declineResponse = function (offerresponse) {
                $http.put('http://localhost:3000/api/offerresponse/' + offerresponse._id,
                    {isResponded: true})
                    .success(function (response) {
                        $rootScope.$broadcast('offerresponseDeclined', offerresponse);
                        showSimpleToast("Offer declined");
                    })
                    .error(function (err) {
                        showSimpleToast("Sorry, something went wrong");
                    });
            };
            function showSimpleToast(txt) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(txt)
                        .position('bottom right')
                        .hideDelay(3000)
                );
            }

            $scope.cancelEditingBuyingoffer = function () {
                showSimpleToast("Editing cancelled");
            };

            function updateBuyingoffer(buyingoffer) {
                $http.put('http://localhost:3000/api/buyingoffers/' + buyingoffer._id, buyingoffer)
                    .success(function (response) {
                        $rootScope.buyingoffer = response;      // store updated value
                        $rootScope.buyingoffer.validationdate = new Date($rootScope.buyingoffer.validationdate);
                        $rootScope.$broadcast('buyingofferUpdated', $rootScope.buyingoffer);
                        showSimpleToast("Request successfully updated");
                        $mdDialog.hide(true);
                    })
                    .error(function (response) {
                        $mdDialog.hide(false);
                        showSimpleToast("error. please try again later");
                    });
            }


            function deleteBuyingoffer(ev) {
                var confirm = $mdDialog.confirm()
                    .title('Are you sure you want to delete this request?')
                    .targetEvent(ev)
                    .ok('Yes')
                    .cancel('Abort');
                $mdDialog.show(confirm).then(function () {
                    var buyingoffer = $rootScope.buyingoffer;
                    $http.delete('http://localhost:3000/api/buyingoffers/' + buyingoffer._id, buyingoffer)
                        .success(function (response) {
                            $rootScope.buyingoffer = null;
                            $scope.OfferSelected = false;
                            $scope.ResponseSelected = false;
                            $rootScope.$broadcast('buyingofferDeleted', buyingoffer);
                            showSimpleToast('Request deleted successfully');
                            return;
                        })
                        .error(function () {
                            showSimpleToast("Error. Try again later");
                        });
                }, function () {
                    showSimpleToast("Delete canceled");
                })
            }


            //--------------------------- DIALOGS---------------------------
            $scope.showOfferAcceptedDialog = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Congratulations')
                        .textContent('Congratulations, you successfully purchased your item. We forwarded you to the "Previous transactions" section in your profile.')
                        .ariaLabel('Offer Accepted Dialog')
                        .ok('OK!')
                        .targetEvent(ev)
                )
            };
            function createOfferresponseDialog(ev) {
                $rootScope.buyingoffer = $scope.buyingoffer;
                var useFullScreen = ( $mdMedia('xs'));
                var toastText;
                $mdDialog.show({
                    controller: "CreateOfferresponseCtrl",
                    templateUrl: 'components/create-offerresponse/create-offerresponse.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    fullscreen: useFullScreen,
                    clickOutsideToClose: true
                })
                    .then(function () {
                        showSimpleToast("Pictures were sent");
                    }, function () {
                        showSimpleToast("Forward aborted");
                    })
            };
            $scope.acceptResponse = function (offerresponse) {
                var confirm = $mdDialog.confirm()
                    .title('Are you sure that you want to accept this offer?')
                    .textContent('Please notice that accepting this offer is binding and all other offers for this item will be declined!')
                    .ok('Yes, I want to buy this item')
                    .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    offerresponse.isTransaction = true;
                    offerresponse.isResponded = true;
                    $http.put('http://localhost:3000/api/offerresponse/' + offerresponse._id,
                        {isTransaction: true, isResponded: true, date:Date.now()})
                        .success(function (response) {
                            angular.forEach($rootScope.offerresponses, function (offerresponseInIteration) {
                                if (offerresponse == offerresponseInIteration || offerresponse.buyingoffer !== offerresponseInIteration.buyingoffer) {
                                }
                                // set all other offerresponses to isResponded = true
                                else {
                                    $http.put('http://localhost:3000/api/offerresponse/' + offerresponseInIteration._id, {isResponded: true})
                                        .success(function (response) {
                                        })
                                        .error(function (response) {
                                            showSimpleToast("Unable to update remaining offers for this request. Please try again later");
                                        });
                                }
                            });
                            $scope.showOfferAcceptedDialog();
                            return $state.go('transactions');
                        })
                        .error(function (err) {
                            showSimpleToast("Sorry, something went wrong");
                        });
                    //updating buyingoffer's status
                    $http.put('http://localhost:3000/api/buyingoffers/' + offerresponse.buyingoffer, {isSold: true})
                        .success(function (response) {
                            $rootScope.$broadcast('buyingofferIsSold', offerresponse);
                        })
                        .error(function (response) {
                            showSimpleToast("error. please try again later");
                        });
                }, function () {
                    showSimpleToast("Item declined");
                });
            };
            function createEditOfferDialog(ev) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    locals: {buyingoffer: $rootScope.buyingoffer},
                    controller: editDialogController,
                    templateUrl: 'components/create-buyingoffer/edit-buyingoffer.html',
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    preserveScope: true
                })
                    .then(function (answer) {
                        if (answer) {
                            //showSimpleToast('Request updated successfully');
                        } else {
                            showSimpleToast('An Error occured!');
                        }
                    }, function () {
                        showSimpleToast('Edit cancelled');
                    });
            }


            // Additional Controllers
            function editDialogController($scope, $mdDialog, buyingoffer, $http, $mdToast, $rootScope, $filter) {
                $scope.buyingoffer = buyingoffer;
                $scope.buyingoffer.validationdate = new Date($scope.buyingoffer.validationdate);
                $scope.validationdate = new Date();
                $scope.minDate = new Date();
                $scope.conditions = [{
                    abbr: "New (original packaging)",
                    short_desc: "Item original and new",
                    long_desc: ""
                }, {
                    abbr: "New (packaging opened or missing)",
                    short_desc: "Item new but not original",
                    long_desc: ""
                }, {
                    abbr: "Barely used",
                    short_desc: "Item barely used",
                    long_desc: ""
                }, {
                    abbr: "Quite used",
                    short_desc: "Item quite used",
                    long_desc: ""
                }, {
                    abbr: "Damaged/broken",
                    short_desc: "Item damaged/broken",
                    long_desc: ""
                }];

                Category.query({parentId:'All Categories'}).$promise.then(function(categories){

                    $scope.categories = $filter('filter')(categories, function (value, index) {
                        return value._id !== 'All Categories';
                    });
                })
                ;
                //$scope.categories = Category.query({parentId: 'All Categories'});


                $scope.getChildren = function (cid) {
                    return Category.query({parentId: cid});
                };

                // ---------------------------HTTP CALLS AND $RESOURCE CALLS---------------------------
                $http.get('http://localhost:3000/api/address')
                    .success(function (response) {
                        $scope.addresses = response;
                    })
                    .error(function (response) {
                        $scope.error = response.message;
                    });

                // ---------------------------functions---------------------------
                $scope.pickAddress = function (address) {
                    $scope.buyingoffer.shippingAddress = address;
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.save = function () {
                    updateBuyingoffer(buyingoffer);
                };
            }
        }
    )
    .filter('byCurrentBuyingoffer', function ($rootScope) {
        return function (input) {
            var out = [];
            angular.forEach(input, function (offerresponse) {
                if (offerresponse.buyingoffer === $rootScope.buyingoffer._id && offerresponse.isResponded == false) {
                    out.push(offerresponse);
                }
            });
            $rootScope.offerresponse = out;
            return out;
        };
    });
