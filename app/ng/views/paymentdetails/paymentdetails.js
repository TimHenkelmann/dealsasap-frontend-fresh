/**
 * Created by yunusemre on 2.07.2016.
 */


'use strict';

angular.module('myApp.payment')
    .constant('paymentdetailsState', {
        name: 'paymentdetails',
        options: {
            parent: 'root',
            url: '/paymentdetails',

            views: {
                'content@root': {
                    templateUrl: 'views/paymentdetails/paymentdetails.html',
                    controller: 'paymentController'
                }
            },

            ncyBreadcrumb: {
                label: "Payment Details"
            }
        }
    })
    .controller('paymentController',
        function($scope, $http, currUser, Offerresponse, Refundclaim, Buyingoffer, $resource, $mdDialog, $rootScope, $mdMedia, $mdToast) {
        $scope.hello="naber";
            $scope.months=[1,2,3,4,5,6,7,8,9,10,11,12];
            var currYear = new Date().getFullYear();
            $scope.years=[currYear,currYear+1,currYear+2,currYear+3,currYear+4,currYear+5,currYear+6];
            $scope.cardtypes=['visa','mastercard'];

           // $scope.payment.user=currUser.getUser()._id;
            var url='http://localhost:3000/api/payment/user/'+currUser.getUser()._id;
            $http({
                method: 'GET',
                url: url
            }).then(function(response) {
                if(response.data[0]!=null) {
                    $scope.payment = response.data[0];
                    $scope.isEmpty=false;
                }else{
                    $scope.isEmpty=true;
                }
            }, function(response) {
                showSimpleToast("error. please try again later");

            });

            $scope.send = function() {
                $http.post('http://localhost:3000/api/payment', {
                    name: $scope.payment.name,
                    cardnumber: $scope.payment.cardnumber,
                    expiremonth: $scope.payment.expiremonth,
                    expireyear: $scope.payment.expireyear,
                    cvvcode: $scope.payment.cvvcode,
                    cardtype: $scope.payment.cardtype,
                    user: currUser.getUser()._id
                })
                    .success(function(response) {
                        $scope.isEmpty=false;
                        $scope.payment = response;      // store updated value
                        showSimpleToast("Your payment details were saved successfully");
                        $mdDialog.hide(true);
                    })
                    .error(function(response) {
                        $mdDialog.hide(false);
                        showSimpleToast("error. please try again later");
                    });
            };

            $scope.update=function() {
                $http.put('http://localhost:3000/api/payment/' + $scope.payment._id, $scope.payment)
                    .success(function (response) {
                        showSimpleToast("Your payment details were updated successfully");
                    })
                    .error(function (response) {
                        showSimpleToast("error. please try again later");
                    });
            }




        });
