/**
 * Created by yunusemre on 27.06.2016.
 */


'use strict';

angular.module('myApp.offerresponse')
    .constant('transactionsState', {
        name: 'transactions',
        options: {
            parent: 'root',
            url: '/transactions',

            views: {
                'content@root': {
                    templateUrl: 'views/transactions/transactions.html',
                    controller: 'transactionsController'
                }
            },

            ncyBreadcrumb: {
                label: "Transactions"
            }
        }
    })
    .controller('transactionsController',
        function ($scope, $http, currUser, Offerresponse, Refundclaim, Buyingoffer,
                  Upload, $resource, $mdDialog, $rootScope, $mdMedia, $mdToast) {
            $scope.uid = currUser.getUser()._id;
            $http.get('http://localhost:3000/api/offerresponse/user/' + currUser.getUser()._id)
                .success(function (data) {
                    $scope.offerresponses = data;
                    angular.forEach($scope.offerresponses, function (offerresponse) {
                        $http.get('http://localhost:3000/api/address/' + offerresponse.buyingoffer)
                            .success(function (address) {
                                offerresponse.address = address;
                            });
                    });
                });

            $scope.getBuyingoffer = function (bid) {
                return Buyingoffer.get({buyingofferId: bid});
                ;
            };

            $scope.checkStatus = function (transaction) {
                return transaction.addressee == currUser.getUser()._id;
            };

            $scope.giveRating = function (ev, transaction) {
                var check;
                $http.get('http://localhost:3000/api/feedback/if_exist/' + transaction._id)
                    .success(function (response) {
                        check = response;
                        if (!(check.exist)) {
                            createFeedbackDialog(ev, transaction);
                        }
                        else {
                            alert('You have already given the feedback for this transaction.');
                        }
                    });
            };

            function showSimpleToast(txt) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(txt)
                        .position('bottom right')
                        .hideDelay(3000)
                );
            };

            function createFeedbackDialog(ev, transaction) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    locals: {ta: transaction},
                    controller: feedbackDialogController,
                    templateUrl: 'views/transactions/rating-dialog.html',
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    preserveScope: true
                })
                    .then(function (answer) {

                        if (answer) {
                            showSimpleToast('Thank you for your feedback.');
                        } else {
                            showSimpleToast('An Error occured!');
                        }
                    }, function () {
                        showSimpleToast('Your feedback has been cancelled.');
                    });
            };

            function feedbackDialogController($scope, $mdDialog, ta, $http, $mdToast, $rootScope) {
                $scope.rating = 3;     // default rating
                var transaction = ta;

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.send = function () {
                    var todayDate = new Date();

                    $http.post('http://localhost:3000/api/feedbacks', {
                        transaction: transaction,
                        title: $scope.title,
                        description: $scope.description,
                        date: todayDate,
                        rating: $scope.rating
                    })
                        .success(function (response) {
                            $scope.feedback = response;      // store updated value
                            $mdDialog.hide(true);
                        })
                        .error(function (response) {
                            $mdDialog.hide(false);
                            showSimpleToast("error. please try again later");
                        });
                };

            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createRefundclaimDialog = function (ev, transaction) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    locals: {ta: transaction},
                    controller: claimrefundDialogController,
                    templateUrl: 'views/transactions/claimrefund-dialog.html',
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    preserveScope: true
                })
                    .then(function (answer) {

                        if (answer) {
                            showSimpleToast('We are sorry that you had to claim a refund. We will process your request as soon as possible!');
                        } else {
                            showSimpleToast('An Error occured!');
                        }
                    }, function () {
                        showSimpleToast('Your refund claim has been cancelled.');
                    });
            };

            function claimrefundDialogController($scope, $mdDialog, ta, $http, $mdToast, $rootScope) {

                $scope.claim = new Refundclaim;
                $scope.claim.transaction = ta;
                $scope.claim.date = Date.now();

                $scope.upload = function () {

                    $scope.currUser = currUser.getUser()._id;
                    Upload.upload({
                        url: 'http://localhost:3000/api/refundclaim',
                        data: {
                            file: $scope.claim.pictures,
                            'message': $scope.claim.message,
                            'buyer': ta.addressee,
                            'seller': ta.sender,
                            'date': Date.now(),
                            'transaction': ta._id
                        }
                    }).then(function (resp) {
                        $mdDialog.hide(true);
                    }).catch(function (resp) {
                        console.log('Error status: ' + resp.status);
                        $mdDialog.hide(false);
                    });


                };

            };


        }
    );
