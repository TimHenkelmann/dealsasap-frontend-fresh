'use strict';

angular.module('myApp.user')
.constant('profileState', {
    name: 'profile',
    options: {
        parent: 'root',
        url: '/profile',

        views: {
            'content@root': {
                templateUrl: 'views/users/profile.html',
                controller: 'profileController'
            }
        },

        ncyBreadcrumb: {
            label: "My Profile"
        }
    }
})
.controller('profileController',
    function ($scope, $http, currUser, User, $resource, $mdMedia,$rootScope, $mdDialog, $mdToast) {
        // get user's profile
        $http.get('http://localhost:3000/users/profile')
        .success(function (response) {
            $scope.profile = response;
        })
        .error(function (response) {
            $scope.error = response.message;
        });

        $scope.addNewAddress = function(ev) {
            createNewAddressDialog(ev);
        };

        $scope.$on('AddressCreated', function (ev, address) {
            $scope.addresses.push(address);
            $scope.showAddress = address;
            
        });
        // get user's addresses
        $http.get('http://localhost:3000/api/address')
        .success(function(response) {
            $scope.addresses = response;
            $scope.showAddress = response[0];
        })
        .error(function(response) {
            $scope.error = response.message;
        });

        $scope.match = function(address) {
            $scope.showAddress = address;
            
        };

        // local function
        function showSimpleToast(txt){
            $mdToast.show(
                $mdToast.simple()
                .textContent(txt)
                .position('bottom right')
                .hideDelay(3000)
            );
        };

        function createNewAddressDialog(ev) {
            var useFullScreen = ( $mdMedia('xs'));
            $mdDialog.show({
                controller: addressDialogController,
                templateUrl: 'views/users/new-address.html',
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                preserveScope:true
            })
            .then(function(address) {

                if (address) {
                    
                    showSimpleToast('Your address has been added successfully');
                } else {
                    showSimpleToast('An Error occured!');
                }
            }, function() {
                showSimpleToast('Operation cancelled.');
            });
        };

        function addressDialogController($scope, $mdDialog, $http, $mdToast, $rootScope) {
            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.save = function() {
                $http.post('http://localhost:3000/api/address', {
                    user: currUser.getUser()._id,
                    houseNumber: $scope.houseno,
                    street: $scope.street,
                    city: $scope.city,
                    postalCode: $scope.postalcode,
                    country: $scope.country
                })
                .success(function(response) {
                    $scope.address = response;      // store updated value
                    $rootScope.$broadcast('AddressCreated', $scope.address );
                    $mdDialog.hide(true);
                })
                .error(function(response) {
                    $mdDialog.hide(false);
                    showSimpleToast("Error. Please try again later");
                });
            };
        };
    }
);
