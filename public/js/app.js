'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['ngFileUpload','ui.router', 'angularUtils.directives.dirPagination', 'myApp.buyingoffers', 'myApp.admin','myApp.home', 'myApp.user', 'myApp.payment', 'payment', 'myApp.offerresponse', 'myApp.myoffer',
                         'templates', 'ncy-angular-breadcrumb', 'ngMaterial', 'ngMessages', 'angular-notification-icons','ngAnimate'])

.config(["$stateProvider", "$urlRouterProvider", "$mdIconProvider", "$resourceProvider", "$httpProvider", "$breadcrumbProvider", "$mdThemingProvider", function($stateProvider, $urlRouterProvider, $mdIconProvider, $resourceProvider, $httpProvider, $breadcrumbProvider, $mdThemingProvider) {

    // For any unmatched url, redirect to /home
    $urlRouterProvider
    .otherwise("/home");


    $stateProvider
    .state('root', {

        abstract: true,
        templateUrl: "views/root/root.html"
    })
    ;

    $mdIconProvider
    .iconSet('content', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg')
    .iconSet('action', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg')
    .iconSet('image', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg')
    .iconSet('editor', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-editor.svg')
    .iconSet('navigation', 'libs/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg');

    //this overrides the defaults actiosn for all $resources
    angular.extend($resourceProvider.defaults.actions, {

        update: {
            method: "PUT"
        }

    });

    $httpProvider.interceptors.push('reqErrInterceptor');
    //auth interceptor
    $httpProvider.interceptors.push('authInterceptor');

    $breadcrumbProvider.setOptions({
        templateUrl:"components/breadcrumbs/breadcrumbs.html",
    });

    $mdThemingProvider.definePalette('mainPalette', {
        '50': '#f4fcfd',
        '100': '#b2e8f2',
        '200': '#82d9ea',
        '300': '#45c7e0',
        '400': '#2bbfdc',
        '500': '#21acc7',
        '600': '#1d95ad',
        '700': '#187f93',
        '800': '#146878',
        '900': '#10515e',
        'A100': '#f4fcfd',
        'A200': '#b2e8f2',
        'A400': '#2bbfdc',
        'A700': '#187f93',
        'contrastDefaultColor': 'light',
        'contrastDarkColors': '50 100 200 300 400 500 A100 A200 A400'
    });

    $mdThemingProvider.theme('default').primaryPalette('mainPalette');
}]);

angular.module('myApp.buyingoffers', ['ngResource', 'ui.router'])

.config(["$stateProvider", "$urlRouterProvider", "buyingofferDetailsState", "buyingofferListState", function ($stateProvider, $urlRouterProvider, buyingofferDetailsState, buyingofferListState) {
    $stateProvider

        .state('buyingoffers', {

            // With abstract set to true, that means this state can not be explicitly activated.
            // It can only be implicitly activated by activating one of its children.
            abstract: true,
            parent: 'root',

            // This abstract state will prepend '/buyingoffers' onto the urls of all its children.
            url: '/buyingoffers'

        })


        // Using a '.' within a state name declares a child within a parent.
        // So you have a new state 'list' within the parent 'buyingoffers' state.
        .state(buyingofferListState.name, buyingofferListState.options)

        .state(buyingofferDetailsState.name, buyingofferDetailsState.options);

}]);

/**
 * Created by yunusemre on 2.07.2016.
 */


angular.module('myApp.payment', ['ngResource', 'ui.router'])

    .config(["$stateProvider", "paymentdetailsState", function ($stateProvider, paymentdetailsState) {
        $stateProvider
            .state(paymentdetailsState.name, paymentdetailsState.options);
    }]);

/**
 * Created by yunusemre on 30.06.2016.
 */

angular.module('myApp.admin', ['ngResource', 'ui.router'])

    .config(["$stateProvider", "adminpanelState", function ($stateProvider, adminpanelState) {
        $stateProvider
            .state(adminpanelState.name, adminpanelState.options);
    }]);

(function(){

    authInterceptor.$inject = ["BASEURL", "auth"];
    angular.module('myApp')
        .factory("authInterceptor", authInterceptor);

    function authInterceptor(BASEURL, auth) {

        function req(config){
            // automatically attach Authorization header
            if(config.url.indexOf(BASEURL) === 0 && auth.isAuthed()) {
                var token = auth.getToken();
                config.headers.Authorization = 'JWT ' + token;
            }

            return config;

        }

        function res(res){

            // If a token was sent back, save it
            if(res && res.config.url.indexOf(BASEURL) === 0 && res.data.token) {
                auth.saveToken(res.data.token);
            }

            return res;

        }

        return {
            request: req,
            response: res
        };
    }

})();
(function(){

    authService.$inject = ["$window"];
    angular.module('myApp')
        .service('auth', authService);

    function authService($window) {

        var self = this;
        this.token;


        this.isAuthed = isAuthed;
        this.parseJwt = parseJwt;
        this.saveToken = saveToken;
        this.getToken = getToken;
        this.deleteToken = deleteToken;

        function saveToken(t) {
            $window.localStorage['jwtToken'] = t;
        }

        function getToken() {
            return $window.localStorage['jwtToken'];
        }

        function deleteToken() {
            $window.localStorage.removeItem('jwtToken');
        }

        function parseJwt(token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        }

        function isAuthed() {

            var token = self.getToken();
            return !!token;
        }
    }

})();

(function(){

    currUserService.$inject = ["BASEURL", "$http", "auth", "$state"];
    angular.module('myApp')
        .service('currUser', currUserService);

    function currUserService(BASEURL, $http, auth, $state) {

        this.register = register;
        this.login = login;
        this.loggedIn = auth.isAuthed;
        this.logout = auth.deleteToken;
        this.getUser = getUser;


        ////////////////

        function register(user, pass, first, last, em) {
            return $http.post(BASEURL + '/signup', {
                username: user,
                password: pass,
                firstName: first,
                lastName: last,
                email: em
            });
        }

        function login(user, pass) {
            return $http.post(BASEURL + '/login', {
                username: user,
                password: pass
            })
            .success(function(response) {
                $state.go('buyingoffers.list');
            });
        }

        function getUser() {
            var token = auth.getToken();
            return token? auth.parseJwt(token).user : {};
        }
    }

})();

/**
 * Created by TimH on 02-Jul-16.
 */

'use strict';

angular.module('myApp.buyingoffers')

    .directive('mvBuyingofferDetail', function() {
        return {
            restrict: 'A',
            scope: {
                buyingoffer: '=',
                sameuser: '=',
                createEditOfferDialog: '=',
                deleteBuyingoffer:'=',
                createOfferresponseDialog:'=',
                processBuyingoffer:'=',
                loggedIn:'='
            },
            templateUrl: 'components/buyingoffer-detail/buyingoffer-detail.html'
        };
    });


/**
 * Created by Anupama on 08-06-2016.
 */

'use strict';

angular.module('myApp.buyingoffers')

    .directive('mvBuyingofferList', function() {
        return {
            restrict: 'A',
            scope: {
                buyingoffer: '='
            },
            templateUrl: 'components/buyingoffer-list/buyingoffer-list.html'
        };
    });


'use strict';

angular.module('myApp.buyingoffers')

    .factory('Buyingoffer', ["$resource", function($resource) {
        return $resource('http://localhost:3000/api/buyingoffers/:buyingofferId', {buyingofferId: '@_id'});

    }]);
'use strict';

angular.module('myApp.buyingoffers')

    .factory('Category', ["$resource", function($resource) {
        return $resource('http://localhost:3000/api/categories/parent/:parentId', {parentId: '@parent'});

    }]);
//taken from http://stackoverflow.com/a/31671397/3200478

angular.module('myApp')
    .directive("compareTo", function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    });

/**
 * One can implmenet a config service if configuration more complex than constants is required
 */
angular.module('myApp')
    .constant("BASEURL", "http://localhost:3000");
angular.module('myApp.buyingoffers')
    .controller('CreateBuyingofferCtrl', ["$scope", "Buyingoffer", "Category", "$mdDialog", "$rootScope", "currUser", "$http", "$filter", function($scope, Buyingoffer, Category, $mdDialog,
                                                  $rootScope, currUser, $http, $filter) {

        $scope.buyingoffer = new Buyingoffer();
        $scope.conditions = [{
            abbr: "New (original packaging)",
            short_desc: "Item original and new",
            long_desc: ""
        },{
            abbr: "New (packaging opened or missing)",
            short_desc: "Item new but not original",
            long_desc: ""
        },
            {
                abbr: "Barely used",
                short_desc: "Item barely used",
                long_desc: ""
            },
            {
                abbr: "Quite used",
                short_desc: "Item quite used",
                long_desc: ""
            },
            {
                abbr: "Damaged/broken",
                short_desc: "Item damaged/broken",
                long_desc: ""
            }];
        $scope.price = 0;

        // get user's addresses
        $http.get('http://localhost:3000/api/address')
        .success(function(response) {
            $scope.addresses = response;
        })
        .error(function(response) {
            $scope.error = response.message;
        });

        $scope.pickAddress = function(address) {
            $scope.buyingoffer.shippingAddress = address;
        };

        $scope.validationdate=new Date();
        $scope.minDate = new Date();

        $scope.save = function() {
            
                    $scope.buyingoffer.user = currUser.getUser()._id;
                    $scope.buyingoffer.$save()
                        .then(function(){
                            $rootScope.$broadcast('buyingofferCreated', $scope.buyingoffer);
                            $mdDialog.hide(true);
                        }).catch(function(){
                        $mdDialog.hide(false);
                    });        
            
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        ///////////////////////////

        Category.query({parentId:'All Categories'}).$promise.then(function(categories){

            $scope.categories = $filter('filter')(categories, function (value, index) {
                return value._id !== 'All Categories';
            });
        })
        ;

        $scope.getChildren = function(cid){
            return Category.query({parentId:cid});
        };
        

    }]);

/**
 * Created by TimH on 22-Jun-16.
 */
angular.module('myApp.buyingoffers')
    .controller('CreateOfferresponseCtrl', ["$scope", "Offerresponse", "Upload", "$mdDialog", "$rootScope", "currUser", function ($scope, Offerresponse, Upload, $mdDialog, $rootScope, currUser) {


        $scope.offerresponse = new Offerresponse();
        $scope.buyingoffer = $rootScope.buyingoffer;
        

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.upload = function () {
            
            $scope.currUser = currUser.getUser()._id;
            Upload.upload({
                url: 'http://localhost:3000/api/offerresponse',
                data: {file: $scope.offerresponse.pictures, 'message': $scope.offerresponse.message, 'sender': $scope.currUser, 'date': Date.now(),'buyingoffer':$scope.buyingoffer._id, 'addressee':$scope.buyingoffer.user}
            }).then(function (resp) {
                $mdDialog.hide(true);
            }).catch( function (resp) {
                console.log('Error status: ' + resp.status);
                $mdDialog.hide(false);
            });
        };


    }]);


'use strict';

angular.module('myApp')

/*
    a generic directive for editable items.

    model:


     */
    .directive('mvEditable', function() {
        return {
            restrict: 'AE',
            transclude:true,
            scope: {
                model: "=",
                saveM: "&onSave",
                cancelM: "&onCancel"
            },

            link: function (scope, iElement, iAttrs, controller, transcludeFn) {
                // this applies the directive' scope to the scope of the transcluded html.
                // further info: http://angular-tips.com/blog/2014/03/transclusion-and-scopes/
                transcludeFn(scope, function(transcludeHtml){ iElement.append(transcludeHtml); });
            },
            controller: ["$scope", "$timeout", function($scope, $timeout){


                $scope.editing = false;
                $scope.startEditing = startEditing;
                $scope.save = save;
                $scope.cancel = cancel;

                //init
                resetValue();


                $scope.$watch('model', function(){
                    if (!$scope.editing) {
                        resetValue()
                    }
                });


                ///////////////////////////////////

                function startEditing() {
                    $scope.editing = true;
                };

                function save(){
                    $scope.editing = false;
                    var changed = ($scope.model != $scope.value);
                    $scope.model = angular.copy($scope.value);

                    //run a digest cycle to update $scope.model before invoking saveM method
                    $timeout( function(){ $scope.saveM({changed:changed}); });
                };

                function cancel() {
                    $scope.editing = false;
                    resetValue();
                    //same as obove. invoke digest cycle
                    $timeout( function(){ $scope.cancelM(); });

                };

                function resetValue() {
                    $scope.value = angular.copy($scope.model);
                }

            }]
        };
    });

angular.module('myApp.home', ['ngResource', 'ui.router', 'myApp.buyingoffers'])

    .config(["$stateProvider", "homeState", function ($stateProvider, homeState) {
        $stateProvider
            .state(homeState.name, homeState.options);
    }])
    .controller('homeController',
        ["$scope", "currUser", "$mdDialog", "$mdMedia", "$mdToast", "$rootScope", "$http", function ($scope, currUser, $mdDialog, $mdMedia, $mdToast, $rootScope, $http) {
            $scope.checkUser = function () {
                if (!currUser.loggedIn()) {
                    // user not logged in yet
                    alert('Please log in first');
                }
                else {
                    //user already logged in
                    alert('User logged in');
                }
            };

            $scope.showAlertLogin = function (ev) {
                // Appending dialog to document.body to cover sidenav in docs app
                // Modal dialogs should fully cover application
                // to prevent interaction outside of dialog
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Login required')
                        .textContent('Please login first or register an account')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(ev)
                );
            };

            $scope.createOffer = function (ev) {
                if (!currUser.loggedIn()) {
                    $scope.showAlertLogin();
                }
                else {
                    $rootScope.invalidateConnectionErrors = true;
                    $http.get('http://localhost:3000/api/payment/user/' + currUser.getUser()._id)
                        .success(function (response) {
                            $rootScope.invalidateConnectionErrors = false;
                            createBuyingofferDialog(ev);
                        })
                        .catch(function (error) {
                            $scope.showDialogCreatePayment();
                            $rootScope.invalidateConnectionErrors = false;
                            return;
                        });
                }
            };

            function createBuyingofferDialog(ev) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    controller: "CreateBuyingofferCtrl",
                    templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    preserveScope: true
                })
                    .then(function (answer) {

                        if (answer) {
                            showSimpleToast('Request saved successfully');
                        } else {
                            showSimpleToast('An Error occured!');
                        }
                    }, function () {
                        showSimpleToast('Request creation cancelled');
                    });
            }
            
            $scope.showDialogCreatePayment = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Payment details missing')
                        .textContent('Unfortunately we could not find any payment details. Please make sure to add them to your profile before creating an offer')
                        .ariaLabel('Payment Details Missing Dialog')
                        .ok('OK')
                        .targetEvent(ev)
                )
            };
            function showSimpleToast(txt) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(txt)
                        .position('bottom right')
                        .hideDelay(3000)
                );
            }
        }]
    );

angular.module('myApp')
    .controller("login", ["$scope", "$rootScope", "currUser", "$mdDialog", function ($scope, $rootScope,currUser, $mdDialog) {
        $scope.username = '';
        $scope.pwd = '';
        $scope.errorText = '';

        $scope.login = login;
        $scope.cancel = cancel;

        function login() {
            currUser.login($scope.username, $scope.password).then(function () {
                $rootScope.$broadcast('userLoggedIn', currUser.getUser());
                $mdDialog.hide();
            }, function (response) {
                if (response.status == 400 || response.status == 401) {
                    $scope.errorText = "Wrong username or password.";
                } else {
                    $scope.errorText = "An unknown error occured. please try again later.";
                }
            });
        }

        function cancel() {
            $mdDialog.cancel();
        }
    }]);

angular.module('myApp.myoffer', ['ngResource', 'ui.router'])

.config(["$stateProvider", "myOfferState", function($stateProvider, myOfferState) {
    $stateProvider
    .state(myOfferState.name, myOfferState.options);
}])


/**
 * Created by yunusemre on 27.06.2016.
 */

angular.module('myApp.offerresponse', ['ngResource', 'ui.router'])

    .config(["$stateProvider", "transactionsState", function ($stateProvider, transactionsState) {
        $stateProvider
            .state(transactionsState.name, transactionsState.options);
    }]);

/**
 * Created by TimH on 22-Jun-16.
 */
'use strict';

angular.module('myApp.offerresponse')

    .factory('Offerresponse', ["$resource", function($resource) {
        return $resource('http://localhost:3000/api/offerresponse/:offerresponseId', {offerresponseId: '@_id'});

    }]);

/**
 * Created by yunusemre on 30.06.2016.
 */


'use strict';

angular.module('myApp.admin')

    .factory('Refundclaim', ["$resource", function($resource) {
        return $resource('http://localhost:3000/api/refundclaim/:refundclaimId', {refundclaimId: '@_id'});

    }]);


angular.module('myApp')
    .controller("register", ["$scope", "currUser", "$mdDialog", function ($scope, currUser, $mdDialog) {
        $scope.username = '';
        $scope.pwd = '';
        $scope.pwdConfirm
        $scope.errorText = '';

        $scope.register = register;
        $scope.cancel = cancel;

        function register() {
            currUser.register($scope.username, $scope.pwd, $scope.firstName, $scope.lastName, $scope.email)
            .then(function () {
                $mdDialog.hide();
            }, function (response) {
                debugger;
                if (response.status == 400 || response.status == 401) {
                    $scope.errorText = "An unknown error occured. please try again later.";
                }
            });
        }

        function cancel() {
            $mdDialog.cancel();
        }
    }]);

(function () {

    reqErrInterceptor.$inject = ["BASEURL", "$injector", "$q", "$rootScope"];
    angular.module('myApp')
        .factory("reqErrInterceptor", reqErrInterceptor);

    function reqErrInterceptor(BASEURL, $injector, $q, $rootScope) {


        return {
            responseError: responseError
        };

        //////////////////////////////

        function responseError(rej) {
            if ([-1, 404].indexOf(rej.status) !== -1) {
                if ($rootScope.invalidateConnectionErrors) {
                }
                else {
                    showAlert({title: 'Connection Error', msg: 'Could not reach the server. Try again later'});
                }
            } else {
                showAlert({title: 'Unknown Error', msg: 'Unknown error. Try again later'});
            }

            return $q.reject(rej);
        }

        function showAlert(opt) {
            //inject manually to resolve circular dependency error
            var $mdDialog = $injector.get('$mdDialog');
            var alert = $mdDialog.alert({
                title: opt.title,
                textContent: opt.msg,
                ok: 'Close'
            });

            $mdDialog.show(alert)

        }

    }

})();
angular.module('myApp')
    .directive('mvToolbar', function () {
        return {
            restrict: "A",
            templateUrl: "components/toolbar/toolbar.html",
            controller: ["$scope", "currUser", "$mdDialog", "$mdMedia", "$mdToast", "$state", "$http", "$rootScope", function ($scope, currUser, $mdDialog, $mdMedia, $mdToast, $state, $http, $rootScope) {

                $scope.user = null;
                $scope.createBuyingofferDialog = createBuyingofferDialog;
                $scope.$on('userLoggedIn', function (ev, user) {
                    $scope.user = user;
                });

                $scope.showLoginDialog = showLoginDialog;
                $scope.showSignupDialog = showSignupDialog;
                $scope.logout = logout;

                $scope.$watch(function () {
                    return currUser.loggedIn();
                }, function (loggedIn) {
                    $scope.loggedIn = loggedIn;
                    if (loggedIn && !$scope.user) {
                        $scope.user = currUser.getUser();
                    }
                });


                $scope.showAlertLogin = function (ev) {
                    // Appending dialog to document.body to cover sidenav in docs app
                    // Modal dialogs should fully cover application
                    // to prevent interaction outside of dialog
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Login required')
                            .textContent('Please login first or register yourself')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('OK')
                            .targetEvent(ev)
                    );
                };
                /////////////////////

                function showLoginDialog() {
                    var useFullScreen = $mdMedia('xs');
                    $mdDialog.show({
                        controller: 'login',
                        templateUrl: 'components/login-dialog/login-dialog.html',
                        clickOutsideToClose: true,
                        fullscreen: useFullScreen
                    });
                };
                function showSignupDialog() {
                    var useFullScreen = $mdMedia('xs');
                    $mdDialog.show({
                        controller: 'register',
                        templateUrl: 'components/register-dialog/register-dialog.html',
                        clickOutsideToClose: true,
                        fullscreen: useFullScreen
                    });
                };

                function logout() {
                    currUser.logout();
                    $state.go('home');      // redirect to homepage when log out
                }

                function showSimpleToast(txt) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(txt)
                            .position('bottom right')
                            .hideDelay(3000)
                    );
                }

                function createBuyingofferDialog(ev) {
                    $rootScope.invalidateConnectionErrors = true;
                    $http.get('http://localhost:3000/api/payment/user/' + currUser.getUser()._id)
                        .success(function (response) {
                            $rootScope.invalidateConnectionErrors = false;
                            if (!currUser.loggedIn()) {      // user not logged in
                                showAlertLogin();
                            }
                            else {      // user already logged in

                                var useFullScreen = ( $mdMedia('xs'));
                                $mdDialog.show({
                                    controller: "CreateBuyingofferCtrl",
                                    templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                                    targetEvent: ev,
                                    clickOutsideToClose: true,
                                    fullscreen: useFullScreen,
                                    preserveScope: true
                                })
                                    .then(function (answer) {

                                        if (answer) {
                                            showSimpleToast('Request saved successfully');
                                        } else {
                                            showSimpleToast('An Error occured!');
                                        }
                                    }, function () {
                                        showSimpleToast('Request creation cancelled');
                                    });
                            }
                        })
                        .catch(function (error) {
                            $scope.showDialogCreatePayment();
                            $rootScope.invalidateConnectionErrors = false;
                            
                        });


                };
                $scope.showDialogCreatePayment = function (ev) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Payment details missing')
                            .textContent('Unfortunately we could not find any payment details. Please make sure to add them to your profile before creating an offer')
                            .ariaLabel('Payment Details Missing Dialog')
                            .ok('OK')
                            .targetEvent(ev)
                    )
                };

            }]
        }
    });

angular.module('myApp.user', ['ngResource', 'ui.router'])

.config(["$stateProvider", "profileState", function ($stateProvider, profileState) {
    $stateProvider
    .state(profileState.name, profileState.options);
}]);


'use strict';

angular.module('myApp.user')

.factory('User', ["$resource", function($resource) {
    return $resource('http://localhost:3000/users/profile');

}]);

/**
* Created by yunusemre on 30.06.2016.
*/


'use strict';

angular.module('myApp.admin')
.constant('adminpanelState', {
    name: 'adminpanel',
    options: {
        parent: 'root',
        url: '/adminpanel',

        views: {
            'content@root': {
                templateUrl: 'views/adminpanel/adminpanel.html',
                controller: 'adminpanelController'
            }
        },

        ncyBreadcrumb: {
            label: "adminpanel"
        }
    }
})
.controller('adminpanelController',
["$scope", "$http", "currUser", "Offerresponse", "Refundclaim", "Buyingoffer", "Upload", "$resource", "$mdDialog", "$rootScope", "$mdMedia", "$mdToast", function($scope, $http, currUser, Offerresponse, Refundclaim, Buyingoffer, Upload, $resource, $mdDialog, $rootScope, $mdMedia, $mdToast) {


    $scope.claims = Refundclaim.query();
    $scope.initialize=function() {
        angular.forEach($scope.claims, function (claim) {
            $http.get('http://localhost:3000/users/getName/' + claim.seller)
            .success(function (response) {
                claim.seller = response;
            })
        });
        angular.forEach($scope.claims, function (claim) {
            $http.get('http://localhost:3000/users/getName/' + claim.buyer)
            .success(function (response) {
                claim.buyer = response;
            })
        });
    }

    $scope.test = "asasdadsd";

    $scope.getTransaction = function (tid) {
        return Offerresponse.get({offerresponseId: tid});
    };
    $scope.getBuyingoffer = function (bid) {
        return Buyingoffer.get({buyingofferId: bid});
    };

    $scope.resolve = function (claim) {
        var updatedclaim = claim;
        updatedclaim.isResolved = true;
        $http.put('http://localhost:3000/api/refundclaim/' + claim._id, {isResolved: true})
        .success(function (response) {
            showSimpleToast("Resolved succesfully");
        })
        .error(function (response) {
            showSimpleToast("error. please try again later");
        });
    };

    $scope.getUserName = function (uid) {
        $http.get('http://localhost:3000/users/getName/' + uid).success(function (data) {
            $scope.test=data.username;
        });
    }

}]);

'use strict';

angular.module('myApp.buyingoffers')

    .constant('buyingofferDetailsState', {
        name: 'buyingoffers.detail',
        options: {
            url: '/{buyingofferId}',

            views: {
                "content@root": {
                    templateUrl: 'views/detail/buyingoffer-detail.html',
                    controller: 'BuyingofferDetailCtrl'
                }
            },

            resolve: {
                //we abuse the resolve feature for eventual redirection
                redirect: function ($state, $stateParams, Buyingoffer, $timeout, $q) {
                    var mid = $stateParams.buyingofferId;
                    var mid = $stateParams.buyingofferId;
                    if (!mid) {
                        //timeout because the transition cannot happen from here
                        $timeout(function () {
                            $state.go("buyingoffers.list");
                        });
                        return $q.reject();
                    }
                }
            },
            ncyBreadcrumb: {
                label: "{{$$childHead.$$childHead.buyingoffer.title}}",
                parent: "buyingoffers.list"
            }

        }
    })
    .controller('BuyingofferDetailCtrl', ["$scope", "Buyingoffer", "Offerresponse", "Category", "$mdToast", "$rootScope", "$mdDialog", "$mdMedia", "$stateParams", "$state", "currUser", "$http", "$filter", function ($scope, Buyingoffer, Offerresponse, Category, $mdToast, $rootScope,$mdDialog, $mdMedia, $stateParams, $state, currUser, $http, $filter) {

        $scope.buyingoffer = Buyingoffer.get({buyingofferId: $stateParams.buyingofferId}, function(buyingoffer) {
            $scope.buyingoffer.validationdate = new Date($scope.buyingoffer.validationdate);
        });
        Category.query({parentId:'All Categories'}).$promise.then(function(categories){

            $scope.categories = $filter('filter')(categories, function (value, index) {
                return value._id !== 'All Categories';
            });
        })
        ;
        //$scope.categories = Category.query({parentId:'All Categories'});
        $scope.getChildren = function(cid){
            return Category.query({parentId:cid});
        };
        $scope.sameuser;
        $scope.mayDelete = false;
        $scope.mayEdit = false;
        $scope.loggedIn = false;
        $scope.deleteBuyingoffer = deleteBuyingoffer;
        $scope.processBuyingoffer = processBuyingoffer;
        $scope.updateBuyingoffer = updateBuyingoffer;
        $scope.createOfferresponseDialog = createOfferresponseDialog;
        $scope.createEditOfferDialog = createEditOfferDialog;
        $scope.authed = false;
        $scope.requirePicture = false;

        $scope.cancelEditingBuyingoffer = function () {
            showSimpleToast("Editing cancelled");
        }

        $scope.$on('buyingofferUpdated', function(ev, buyingoffer){
            $scope.$evalAsync(function () {
                $scope.buyingoffer = buyingoffer;
            });
        });
        $scope.buyingoffer.$promise.then(function () {
            //$scope.mayDelete = $scope.buyingoffer.user && $scope.buyingoffer.user == currUser.getUser()._id;
            $scope.sameuser = $scope.buyingoffer.user && $scope.buyingoffer.user == currUser.getUser()._id;
        });

        $scope.$watch(function () {
            return currUser.loggedIn();
        }, function (loggedIn) {
            if (!loggedIn) {
                $scope.mayDelete = false;
                $scope.mayEdit = false;
                $scope.loggedIn = false;
            } else {
                $http.get('http://localhost:3000/api/address')
                    .success(function(response) {
                        $scope.addresses = response;
                    })
                    .error(function(response) {
                        $scope.error = response.message;
                    });
                $scope.loggedIn = true;
                $scope.mayEdit = $scope.buyingoffer.user == currUser.getUser()._id;
                $scope.mayDelete = $scope.buyingoffer.user == currUser.getUser()._id;
            }
        })

        // For offer editing section
        $scope.conditions = [{
            abbr: "New (original packaging)",
            short_desc: "Item original and new",
            long_desc: ""
        },{
            abbr: "New (packaging opened or missing)",
            short_desc: "Item new but not original",
            long_desc: ""
        },
        {
            abbr: "Barely used",
            short_desc: "Item barely used",
            long_desc: ""
        },
        {
            abbr: "Quite used",
            short_desc: "Item quite used",
            long_desc: ""
        },
        {
            abbr: "Damaged/broken",
            short_desc: "Item damaged/broken",
            long_desc: ""
        }];

        

        $scope.pickAddress = function(address) {
            $scope.buyingoffer.shippingAddress = address;
        };

        $scope.validationdate = new Date();
        $scope.minDate = new Date();

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.save = function() {
            $scope.buyingoffer.user = currUser.getUser()._id;
            $scope.updateBuyingoffer();
        };

        ////////////////////
        function updateBuyingoffer() {
            $http.put('http://localhost:3000/api/buyingoffers/' + $stateParams.buyingofferId, $scope.buyingoffer)
                .success(function(response) {
                    $scope.buyingoffer = response;      // store updated value
                    $scope.buyingoffer.validationdate = new Date($scope.buyingoffer.validationdate);
                    $rootScope.$broadcast('buyingofferUpdated', $scope.buyingoffer);
                    $mdDialog.hide(true);
                })
                .error(function(response) {
                    $mdDialog.hide(false);
                    showSimpleToast("error. please try again later");
                });
        }

        function deleteBuyingoffer(ev) {

            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to delete this request?')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Abort');

            var toastText;
            $mdDialog.show(confirm).then(function () {
                return $scope.buyingoffer.$remove().then(function () {
                    return $state.go('buyingoffers.list');
                }).then(function () {
                    showSimpleToast('Request deleted successfully');
                }, function () {
                    showSimpleToast("Error. Try again later");
                });
            }, function () {
                showSimpleToast("Delete aborted");
            })
        }

        function processBuyingoffer(ev) {

            var confirm = $mdDialog.confirm()
                .title('Do you really want to sell this item?')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Abort');

            var toastText;
            $mdDialog.show(confirm).then(function () {
                var offerresponse= new Offerresponse();
                offerresponse.buyingoffer=$scope.buyingoffer;
                offerresponse.sender=currUser.getUser()._id;
                offerresponse.addressee=$scope.buyingoffer.user;
                offerresponse.isTransaction=true;
                offerresponse.date=Date.now();

                offerresponse.$save()
                    .then(function(){
                        $mdDialog.hide(true);
                        showSimpleToast("Sold successfully. Please see the shipping address in previous transactions");
                    }).catch(function(){
                    $mdDialog.hide(false);
                });

                $http.put('http://localhost:3000/api/buyingoffers/' + $scope.buyingoffer._id, {isSold: true})
                    .success(function (response) {
                    })
                    .error(function (response) {
                        showSimpleToast("error. please try again later");
                    });


            }, function () {
                showSimpleToast("Forward aborted");
            })
        }

        function createOfferresponseDialog(ev) {
            $rootScope.buyingoffer = $scope.buyingoffer;
            var useFullScreen = ( $mdMedia('xs'));
            var toastText;
            $mdDialog.show({
                controller: "CreateOfferresponseCtrl",
                templateUrl: 'components/create-offerresponse/create-offerresponse.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                fullscreen: useFullScreen,
                clickOutsideToClose: true
            })
                .then(function () {
                    showSimpleToast("Pictures were sent. You can check the status of your offer in \"My Requests/Offer\"");
                }, function () {
                    showSimpleToast("Forward aborted");
                })


        }


        function showSimpleToast(txt) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }

        function createEditOfferDialog(ev, transaction) {
            var useFullScreen = ( $mdMedia('xs'));
            $mdDialog.show({
                controller: "BuyingofferDetailCtrl",
                templateUrl: 'components/create-buyingoffer/edit-buyingoffer.html',
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                preserveScope:true
            })
            .then(function(answer) {

                if (answer) {
                    showSimpleToast('Request updated successfully');
                } else {
                    showSimpleToast('An Error occured!');
                }
            }, function() {
                showSimpleToast('Edit cancelled');
            });
        }

    }]);

'use strict';

angular.module('myApp.home')
.constant('homeState', {
    name: 'home',
    options: {
        parent: 'root',
        url: '/home',

        views: {
            'content@root': {
                templateUrl: 'views/home/home.html',
                controller: 'homeController'
            }
        },

        ncyBreadcrumb: {
            label: "Home"
        }
    }
})

'use strict';

angular.module('myApp.buyingoffers')

    .constant('buyingofferListState', {
        name: 'buyingoffers.list',
        options: {

            // Using an empty url means that this child state will become active
            // when its parent's url is navigated to. Urls of child states are
            // automatically appended to the urls of their parent. So this state's
            // url is '/buyingoffers' (because '/buyingoffers' + '').
            url: '',

            views: {
                'content@root': {
                    templateUrl: 'views/list/buyingoffer-list.html',
                    controller: 'BuyingofferListCtrl',
                },
                'outside@root': {
                    templateUrl: 'views/list/buyingoffer-list-buttons.html',
                    controller: 'buyingofferListButtonCtrl'
                }
            },

            ncyBreadcrumb: {
                label: "Browse"
            }

        }

    })

    .controller('BuyingofferListCtrl', ["$scope", "Buyingoffer", "Category", "$http", "$resource", function ($scope, Buyingoffer, Category, $http, $resource) {

        $scope.buyingoffers = Buyingoffer.query();

        this.category = "home";

        Category.query({parentId: 'All Categories'}).$promise.then(function (categories) {
            $scope.categories = categories;
            $scope.categories[0].makePrimary = 'md-primary';
        });

        $scope.getChildren = function (cid) {
            return Category.query({parentId: cid._id});
        };

        $scope.updateData = function (catgry) {
            
            $http.get('http://localhost:3000/api/buyingoffers/category/' + catgry._id).success(function (data) {
                $scope.buyingoffers = data;
                angular.forEach($scope.categories, function (category) {
                    category.makePrimary = '';
                });

                    catgry.makePrimary = 'md-primary';

            });

        };

        $scope.$on('buyingofferCreated', function (ev, buyingoffer) {
            $scope.buyingoffers.push(buyingoffer);
        });


    }])

    .controller('buyingofferListButtonCtrl', ["$scope", "$mdMedia", "$mdDialog", "$mdToast", "currUser", "$rootScope", "$http", function ($scope, $mdMedia, $mdDialog, $mdToast, currUser, $rootScope, $http) {

        $scope.createBuyingofferDialog = createBuyingofferDialog;
        $scope.authed = false;

        $scope.$watch(function () {
            return currUser.loggedIn();
        }, function (loggedIn) {
            $scope.authed = loggedIn;
        });

        ////////////////////////////////////

        function createBuyingofferDialog(ev) {
            $rootScope.invalidateConnectionErrors = true;
            $http.get('http://localhost:3000/api/payment/user/' + currUser.getUser()._id)
                .success(function (response) {
                    $rootScope.invalidateConnectionErrors = false;
                    var useFullScreen = ( $mdMedia('xs'));
                    $mdDialog.show({
                        controller: "CreateBuyingofferCtrl",
                        templateUrl: 'components/create-buyingoffer/create-buyingoffer.html',
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: useFullScreen,
                        preserveScope: true
                    })
                        .then(function (answer) {

                            if (answer) {
                                showSimpleToast('Request saved successfully');
                            } else {
                                showSimpleToast('An Error occured!');
                            }
                        }, function () {
                            showSimpleToast('Request creation cancelled');
                        });
                })
                .catch(function (error) {
                    $scope.showDialogCreatePayment();
                    $rootScope.invalidateConnectionErrors = false;
                    return;
                });
        };
        $scope.showDialogCreatePayment = function (ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Payment details missing')
                    .textContent('Unfortunately we could not find any payment details. Please make sure to add them to your profile before creating an offer')
                    .ariaLabel('Payment Details Missing Dialog')
                    .ok('OK')
                    .targetEvent(ev)
            )
        };

        function showSimpleToast(txt) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }
    }]);

'use strict';


angular.module('myApp.myoffer')
    .constant('myOfferState', {
        name: 'buyingoffers.myoffer',
        options: {
            parent: 'root',
            url: '/myoffer',
            views: {
                'content@root': {
                    templateUrl: 'views/myoffer/myoffer.html',
                    controller: 'MyOfferController'
                }
            },

            ncyBreadcrumb: {
                label: "My Requests/Offers"
            }
        }
    })
    .controller('MyOfferController',
        ["$scope", "$http", "currUser", "$rootScope", "$mdToast", "$mdDialog", "$mdMedia", "Buyingoffer", "Offerresponse", "Category", "$stateParams", "$state", "$filter", function ($scope, $http, currUser, $rootScope, $mdToast, $mdDialog,
                  $mdMedia, Buyingoffer, Offerresponse, Category, $stateParams, $state, $filter) {


            editDialogController.$inject = ["$scope", "$mdDialog", "buyingoffer", "$http", "$mdToast", "$rootScope", "$filter"];
            $scope.OfferSelected = false;
            $scope.mayDelete = true;    // always true because it's in that user's page
            $scope.mayEdit = true;
            $scope.deleteBuyingoffer = deleteBuyingoffer;
            $scope.updateBuyingoffer = updateBuyingoffer;
            $scope.createEditOfferDialog = createEditOfferDialog;
            $scope.createOfferresponseDialog = createOfferresponseDialog;
            $scope.authed = false;


            //---------------------------HTTP CALLS AND $RESOURCE CALLS---------------------------
            // Get all buyingoffers from this user
            $http.get('http://localhost:3000/api/buyingoffers/user/' + currUser.getUser()._id)
                .success(function (response) {
                    $scope.buyingoffers = $filter('filter')(response, {isSold: false});
                    angular.forEach($scope.buyingoffers, function (buyingoffer) {
                        $http.get('http://localhost:3000/api/offerresponse/buyingoffer/' + buyingoffer._id)
                            .success(function (response) {
                                if (response == '') {
                                    buyingoffer.haveNewResponse = '';
                                    buyingoffer.hasResponsesClass = '';
                                }
                                else {
                                    buyingoffer.haveNewResponse = '';
                                    buyingoffer.hasResponsesClass = '';
                                    var keepgoing = true;
                                    angular.forEach(response, function (offerresponse) {
                                        if (keepgoing) {
                                            if (offerresponse.isResponded == false) {
                                                buyingoffer.haveNewResponse = '(New Offer!)';
                                                buyingoffer.hasResponsesClass = 'md-primary';
                                                keepgoing = false;
                                            }
                                        }
                                    });
                                }
                            });
                    });
                })
                .error(function (response) {
                    alert('error');
                    $scope.error = response.message;
                });

            // Get all offerresponses of an item that this user request
            $http.get('http://localhost:3000/api/offerresponse/Addressee/' + currUser.getUser()._id)
                .success(function (data) {
                    $rootScope.offerresponses = data;
                    angular.forEach($rootScope.offerresponses, function (offerresponse) {
                        $http.get('http://localhost:3000/users/getName/' + offerresponse.sender)
                            .success(function (response) {
                                offerresponse.sender = response;
                            });
                    });
                })
                .error(function (response) {
                    alert('error');
                    $scope.error = response.message;
                });

            // Get all offerresponses which this user sent
            $http.get('http://localhost:3000/api/offerresponse/Sender/' + currUser.getUser()._id)
                .success(function (data) {
                    $scope.senderresponses = data;
                    angular.forEach($scope.senderresponses, function (senderresponse) {
                        $http.get('http://localhost:3000/api/buyingoffers/' + senderresponse.buyingoffer)
                            .success(function (response) {
                                senderresponse.buyingoffer = response;
                            });
                    });
                })
                .error(function (response) {
                    alert('error');
                    $scope.error = response.message;
                });


            //---------------------------$on---------------------------
            $scope.$on('buyingofferCreated', function (ev, buyingoffer) {
                $scope.buyingoffers.push(buyingoffer);
            });
            $scope.$on('buyingofferDeleted', function (ev, buyingoffer) {
                $scope.buyingoffers = $filter('filter')($scope.buyingoffers, function (value, index) {
                    return value._id !== buyingoffer._id;
                });
            });
            $scope.$on('offerresponseDeclined', function (ev, offerresponse) {
                $rootScope.offerresponses = $filter('filter')($rootScope.offerresponses, function (value, index) {
                    return value._id !== offerresponse._id;
                });
                $rootScope.buyingoffer.haveNewResponse = '';
                $rootScope.buyingoffer.hasResponsesClass = '';
            });
            $scope.$on('buyingofferIsSold', function (ev, offerresponse) {
                $scope.buyingoffers = $filter('filter')($scope.buyingoffers, function (value, index) {
                    return value._id !== offerresponse.buyingoffer;
                });
                $scope.OfferSelected = false;
            });
            $scope.$on('buyingofferUpdated', function (ev, buyingoffer) {
                $scope.$evalAsync(function () {
                    $rootScope.buyingoffer = buyingoffer;
                });
            });


            //---------------------------functions---------------------------
            $scope.setBuyingoffer = function (selectedbuyingoffer) {
                $rootScope.buyingoffer = selectedbuyingoffer;
                $rootScope.sameuser = (selectedbuyingoffer.user === currUser.getUser()._id);
                $scope.OfferSelected = true;
                $scope.ResponseSelected = false;
            };
            $scope.setOfferresponse = function (selectedresponse) {
                $rootScope.buyingoffer = selectedresponse.buyingoffer;
                $rootScope.sameuser = (selectedresponse.buyingoffer.user === currUser.getUser()._id);
                $scope.OfferSelected = true;
                $scope.ResponseSelected = true;
                if (!selectedresponse.isResponded) {
                    $scope.status = 'Pending';
                    $scope.statusColor = 'orange';
                }
                else if (!selectedresponse.isTransaction) {
                    $scope.status = 'Declined';
                    $scope.statusColor = 'red';
                }
                else {
                    $scope.status = 'Accepted, please go to "previous transactions" in your profile to see more details';
                    $scope.statusColor = 'green';
                }
            };
            $scope.hasResponses = function (currentBuyingoffer) {
                for (var offerresponse in $rootScope.offerresponses) {
                    if (offerresponse.buyingoffer === currentBuyingoffer._id) {
                        return true;
                    }
                }
            };
            $scope.declineResponse = function (offerresponse) {
                $http.put('http://localhost:3000/api/offerresponse/' + offerresponse._id,
                    {isResponded: true})
                    .success(function (response) {
                        $rootScope.$broadcast('offerresponseDeclined', offerresponse);
                        showSimpleToast("Item declined");
                    })
                    .error(function (err) {
                        showSimpleToast("Sorry, something went wrong");
                    });
            };
            function showSimpleToast(txt) {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent(txt)
                        .position('bottom right')
                        .hideDelay(3000)
                );
            }

            $scope.cancelEditingBuyingoffer = function () {
                showSimpleToast("Editing cancelled");
            };

            function updateBuyingoffer(buyingoffer) {
                $http.put('http://localhost:3000/api/buyingoffers/' + buyingoffer._id, buyingoffer)
                    .success(function (response) {
                        $rootScope.buyingoffer = response;      // store updated value
                        $rootScope.buyingoffer.validationdate = new Date($rootScope.buyingoffer.validationdate);
                        $rootScope.$broadcast('buyingofferUpdated', $rootScope.buyingoffer);
                        showSimpleToast("Request successfully updated");
                        $mdDialog.hide(true);
                    })
                    .error(function (response) {
                        $mdDialog.hide(false);
                        showSimpleToast("error. please try again later");
                    });
            }


            function deleteBuyingoffer(ev) {
                var confirm = $mdDialog.confirm()
                    .title('Are you sure you want to delete this request?')
                    .targetEvent(ev)
                    .ok('Yes')
                    .cancel('Abort');
                $mdDialog.show(confirm).then(function () {
                    var buyingoffer = $rootScope.buyingoffer;
                    $http.delete('http://localhost:3000/api/buyingoffers/' + buyingoffer._id, buyingoffer)
                        .success(function (response) {
                            $rootScope.buyingoffer = null;
                            $scope.OfferSelected = false;
                            $scope.ResponseSelected = false;
                            $rootScope.$broadcast('buyingofferDeleted', buyingoffer);
                            showSimpleToast('Request deleted successfully');
                            return;
                        })
                        .error(function () {
                            showSimpleToast("Error. Try again later");
                        });
                }, function () {
                    showSimpleToast("Delete canceled");
                })
            }


            //--------------------------- DIALOGS---------------------------
            $scope.showOfferAcceptedDialog = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Congratulations')
                        .textContent('Congratulations, you successfully purchased your item. You can find the item and the payment details under "Previous transactions" in your profile.')
                        .ariaLabel('Offer Accepted Dialog')
                        .ok('OK!')
                        .targetEvent(ev)
                )
            };
            function createOfferresponseDialog(ev) {
                $rootScope.buyingoffer = $scope.buyingoffer;
                var useFullScreen = ( $mdMedia('xs'));
                var toastText;
                $mdDialog.show({
                    controller: "CreateOfferresponseCtrl",
                    templateUrl: 'components/create-offerresponse/create-offerresponse.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    fullscreen: useFullScreen,
                    clickOutsideToClose: true
                })
                    .then(function () {
                        showSimpleToast("Pictures were sent");
                    }, function () {
                        showSimpleToast("Forward aborted");
                    })
            };
            $scope.acceptResponse = function (offerresponse) {
                var confirm = $mdDialog.confirm()
                    .title('Are you sure that you want to accept this offer?')
                    .textContent('Please notice that accepting this offer is binding and all other offers for this item will be declined!')
                    .ok('Yes, I want to buy this item')
                    .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    offerresponse.isTransaction = true;
                    offerresponse.isResponded = true;
                    $http.put('http://localhost:3000/api/offerresponse/' + offerresponse._id,
                        {isTransaction: true, isResponded: true})
                        .success(function (response) {
                            angular.forEach($rootScope.offerresponses, function (offerresponseInIteration) {
                                if (offerresponse == offerresponseInIteration) {
                                }
                                // set all other offerresponses to isResponded = true
                                else {
                                    $http.put('http://localhost:3000/api/offerresponse/' + offerresponseInIteration._id, {isResponded: true})
                                        .success(function (response) {
                                        })
                                        .error(function (response) {
                                            showSimpleToast("Unable to update remaining offers for this request. Please try again later");
                                        });
                                }
                            });
                            $scope.showOfferAcceptedDialog();
                            return $state.go('transactions');
                        })
                        .error(function (err) {
                            showSimpleToast("Sorry, something went wrong");
                        });
                    //updating buyingoffer's status
                    $http.put('http://localhost:3000/api/buyingoffers/' + offerresponse.buyingoffer, {isSold: true})
                        .success(function (response) {
                            $rootScope.$broadcast('buyingofferIsSold', offerresponse);
                        })
                        .error(function (response) {
                            showSimpleToast("error. please try again later");
                        });
                }, function () {
                    showSimpleToast("Item declined");
                });
            };
            function createEditOfferDialog(ev) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    locals: {buyingoffer: $rootScope.buyingoffer},
                    controller: editDialogController,
                    templateUrl: 'components/create-buyingoffer/edit-buyingoffer.html',
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    preserveScope: true
                })
                    .then(function (answer) {
                        if (answer) {
                            //showSimpleToast('Request updated successfully');
                        } else {
                            showSimpleToast('An Error occured!');
                        }
                    }, function () {
                        showSimpleToast('Edit cancelled');
                    });
            }


            // Additional Controllers
            function editDialogController($scope, $mdDialog, buyingoffer, $http, $mdToast, $rootScope, $filter) {
                $scope.buyingoffer = buyingoffer;
                $scope.buyingoffer.validationdate = new Date($scope.buyingoffer.validationdate);
                $scope.validationdate = new Date();
                $scope.minDate = new Date();
                $scope.conditions = [{
                    abbr: "New (original packaging)",
                    short_desc: "Item original and new",
                    long_desc: ""
                }, {
                    abbr: "New (packaging opened or missing)",
                    short_desc: "Item new but not original",
                    long_desc: ""
                }, {
                    abbr: "Barely used",
                    short_desc: "Item barely used",
                    long_desc: ""
                }, {
                    abbr: "Quite used",
                    short_desc: "Item quite used",
                    long_desc: ""
                }, {
                    abbr: "Damaged/broken",
                    short_desc: "Item damaged/broken",
                    long_desc: ""
                }];

                Category.query({parentId:'All Categories'}).$promise.then(function(categories){

                    $scope.categories = $filter('filter')(categories, function (value, index) {
                        return value._id !== 'All Categories';
                    });
                })
                ;
                //$scope.categories = Category.query({parentId: 'All Categories'});


                $scope.getChildren = function (cid) {
                    return Category.query({parentId: cid});
                };

                // ---------------------------HTTP CALLS AND $RESOURCE CALLS---------------------------
                $http.get('http://localhost:3000/api/address')
                    .success(function (response) {
                        $scope.addresses = response;
                    })
                    .error(function (response) {
                        $scope.error = response.message;
                    });

                // ---------------------------functions---------------------------
                $scope.pickAddress = function (address) {
                    $scope.buyingoffer.shippingAddress = address;
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.save = function () {
                    updateBuyingoffer(buyingoffer);
                };
            }
        }]
    )
    .filter('byCurrentBuyingoffer', ["$rootScope", function ($rootScope) {
        return function (input) {
            var out = [];
            angular.forEach(input, function (offerresponse) {
                if (offerresponse.buyingoffer === $rootScope.buyingoffer._id && offerresponse.isResponded == false) {
                    out.push(offerresponse);
                }
            });
            $rootScope.offerresponse = out;
            return out;
        };
    }]);

/**
 * Created by yunusemre on 2.07.2016.
 */


'use strict';

angular.module('myApp.payment')
    .constant('paymentdetailsState', {
        name: 'paymentdetails',
        options: {
            parent: 'root',
            url: '/paymentdetails',

            views: {
                'content@root': {
                    templateUrl: 'views/paymentdetails/paymentdetails.html',
                    controller: 'paymentController'
                }
            },

            ncyBreadcrumb: {
                label: "Payment Details"
            }
        }
    })
    .controller('paymentController',
        ["$scope", "$http", "currUser", "Offerresponse", "Refundclaim", "Buyingoffer", "$resource", "$mdDialog", "$rootScope", "$mdMedia", "$mdToast", function($scope, $http, currUser, Offerresponse, Refundclaim, Buyingoffer, $resource, $mdDialog, $rootScope, $mdMedia, $mdToast) {
        $scope.hello="naber";
            $scope.months=[1,2,3,4,5,6,7,8,9,10,11,12];
            var currYear = new Date().getFullYear();
            $scope.years=[currYear,currYear+1,currYear+2,currYear+3,currYear+4,currYear+5,currYear+6];
            $scope.cardtypes=['visa','mastercard'];

           // $scope.payment.user=currUser.getUser()._id;
            var url='http://localhost:3000/api/payment/user/'+currUser.getUser()._id;
            $http({
                method: 'GET',
                url: url
            }).then(function(response) {
                if(response.data[0]!=null) {
                    $scope.payment = response.data[0];
                    $scope.isEmpty=false;
                }else{
                    $scope.isEmpty=true;
                }
            }, function(response) {
                showSimpleToast("error. please try again later");

            });

            $scope.send = function() {
                $http.post('http://localhost:3000/api/payment', {
                    name: $scope.payment.name,
                    cardnumber: $scope.payment.cardnumber,
                    expiremonth: $scope.payment.expiremonth,
                    expireyear: $scope.payment.expireyear,
                    cvvcode: $scope.payment.cvvcode,
                    cardtype: $scope.payment.cardtype,
                    user: currUser.getUser()._id
                })
                    .success(function(response) {
                        $scope.isEmpty=false;
                        $scope.payment = response;      // store updated value
                        showSimpleToast("Your payment details were saved successfully");
                        $mdDialog.hide(true);
                    })
                    .error(function(response) {
                        $mdDialog.hide(false);
                        showSimpleToast("error. please try again later");
                    });
            };

            $scope.update=function() {
                $http.put('http://localhost:3000/api/payment/' + $scope.payment._id, $scope.payment)
                    .success(function (response) {
                        showSimpleToast("Your payment details were updated successfully");
                    })
                    .error(function (response) {
                        showSimpleToast("error. please try again later");
                    });
            }




        }]);

/**
* Created by yunusemre on 27.06.2016.
*/


'use strict';

angular.module('myApp.offerresponse')
    .constant('transactionsState', {
        name: 'transactions',
        options: {
            parent: 'root',
            url: '/transactions',

            views: {
                'content@root': {
                    templateUrl: 'views/transactions/transactions.html',
                    controller: 'transactionsController'
                }
            },

            ncyBreadcrumb: {
                label: "Transactions"
            }
        }
    })
    .controller('transactionsController',
    ["$scope", "$http", "currUser", "Offerresponse", "Refundclaim", "Buyingoffer", "Upload", "$resource", "$mdDialog", "$rootScope", "$mdMedia", "$mdToast", function($scope, $http, currUser, Offerresponse, Refundclaim, Buyingoffer,
        Upload, $resource, $mdDialog, $rootScope, $mdMedia, $mdToast) {
            feedbackDialogController.$inject = ["$scope", "$mdDialog", "ta", "$http", "$mdToast", "$rootScope"];
            claimrefundDialogController.$inject = ["$scope", "$mdDialog", "ta", "$http", "$mdToast", "$rootScope"];
            $scope.uid=currUser.getUser()._id;
            $http.get('http://localhost:3000/api/offerresponse/user/' + currUser.getUser()._id)
            .success(function(data) {
                $scope.offerresponses = data;
                angular.forEach($scope.offerresponses, function(offerresponse) {
                    $http.get('http://localhost:3000/api/address/' + offerresponse.buyingoffer)
                    .success(function(address) {
                        offerresponse.address = address;
                    });
                });
            });

            $scope.getBuyingoffer = function(bid){
                return Buyingoffer.get({buyingofferId: bid});;
            };

            $scope.checkStatus = function(transaction){
                return transaction.addressee==currUser.getUser()._id;
            };

            $scope.giveRating = function(ev, transaction) {
                var check;
                $http.get('http://localhost:3000/api/feedback/if_exist/' + transaction._id)
                .success(function(response) {
                    check = response;
                    if (!(check.exist)) {
                        createFeedbackDialog(ev, transaction);
                    }
                    else {
                        alert('You have already given the feedback for this transaction.');
                    }
                });
            };

            function showSimpleToast(txt){
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
                );
            };

            function createFeedbackDialog(ev, transaction) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    locals: { ta: transaction },
                    controller: feedbackDialogController,
                    templateUrl: 'views/transactions/rating-dialog.html',
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen,
                    preserveScope:true
                })
                .then(function(answer) {

                    if (answer) {
                        showSimpleToast('Thank you for your feedback.');
                    } else {
                        showSimpleToast('An Error occured!');
                    }
                }, function() {
                    showSimpleToast('Your feedback has been cancelled.');
                });
            };

            function feedbackDialogController($scope, $mdDialog, ta, $http, $mdToast, $rootScope) {
                $scope.rating = 3;     // default rating
                var transaction = ta;

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.send = function() {
                    var todayDate = new Date();

                    $http.post('http://localhost:3000/api/feedbacks', {
                        transaction: transaction,
                        title: $scope.title,
                        description: $scope.description,
                        date: todayDate,
                        rating: $scope.rating
                    })
                    .success(function(response) {
                        $scope.feedback = response;      // store updated value
                        $mdDialog.hide(true);
                    })
                    .error(function(response) {
                        $mdDialog.hide(false);
                        showSimpleToast("error. please try again later");
                    });
                };

            };


            $scope.createRefundclaimDialog = function(ev, transaction) {
                var useFullScreen = ( $mdMedia('xs'));
                $mdDialog.show({
                    locals: { ta: transaction },
                    controller: claimrefundDialogController,
                    templateUrl: 'views/transactions/claimrefund-dialog.html',
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen,
                    preserveScope:true
                })
                .then(function(answer) {

                    if (answer) {
                        showSimpleToast('Thank you for your feedback.');
                    } else {
                        showSimpleToast('An Error occured!');
                    }
                }, function() {
                    showSimpleToast('Your feedback has been cancelled.');
                });
            };

            function claimrefundDialogController($scope, $mdDialog, ta, $http, $mdToast, $rootScope) {

                $scope.claim=new Refundclaim;
                $scope.claim.transaction=ta;
                $scope.claim.date=Date.now();

                $scope.upload = function () {

                    $scope.currUser = currUser.getUser()._id;
                    Upload.upload({
                        url: 'http://localhost:3000/api/refundclaim',
                        data: {file: $scope.claim.pictures, 'message': $scope.claim.message, 'buyer': ta.addressee, 'seller': ta.sender, 'date': Date.now(),'transaction':ta._id}
                    }).then(function (resp) {
                        $mdDialog.hide(true);
                    }).catch( function (resp) {
                        console.log('Error status: ' + resp.status);
                        $mdDialog.hide(false);
                    });


                };

            };
            

        }]
);

'use strict';

angular.module('myApp.user')
.constant('profileState', {
    name: 'profile',
    options: {
        parent: 'root',
        url: '/profile',

        views: {
            'content@root': {
                templateUrl: 'views/users/profile.html',
                controller: 'profileController'
            }
        },

        ncyBreadcrumb: {
            label: "My Profile"
        }
    }
})
.controller('profileController',
    ["$scope", "$http", "currUser", "User", "$resource", "$mdMedia", "$rootScope", "$mdDialog", "$mdToast", function ($scope, $http, currUser, User, $resource, $mdMedia,$rootScope, $mdDialog, $mdToast) {
        // get user's profile
        addressDialogController.$inject = ["$scope", "$mdDialog", "$http", "$mdToast", "$rootScope"];
        $http.get('http://localhost:3000/users/profile')
        .success(function (response) {
            $scope.profile = response;
        })
        .error(function (response) {
            $scope.error = response.message;
        });

        $scope.addNewAddress = function(ev) {
            createNewAddressDialog(ev);
        };

        $scope.$on('AddressCreated', function (ev, address) {
            $scope.addresses.push(address);
            $scope.showAddress = address;
            
        });
        // get user's addresses
        $http.get('http://localhost:3000/api/address')
        .success(function(response) {
            $scope.addresses = response;
            $scope.showAddress = response[0];
        })
        .error(function(response) {
            $scope.error = response.message;
        });

        $scope.match = function(address) {
            $scope.showAddress = address;
            
        };

        // local function
        function showSimpleToast(txt){
            $mdToast.show(
                $mdToast.simple()
                .textContent(txt)
                .position('bottom right')
                .hideDelay(3000)
            );
        };

        function createNewAddressDialog(ev) {
            var useFullScreen = ( $mdMedia('xs'));
            $mdDialog.show({
                controller: addressDialogController,
                templateUrl: 'views/users/new-address.html',
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                preserveScope:true
            })
            .then(function(address) {

                if (address) {
                    
                    showSimpleToast('Your address has been added successfully');
                } else {
                    showSimpleToast('An Error occured!');
                }
            }, function() {
                showSimpleToast('Operation cancelled.');
            });
        };

        function addressDialogController($scope, $mdDialog, $http, $mdToast, $rootScope) {
            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.save = function() {
                $http.post('http://localhost:3000/api/address', {
                    user: currUser.getUser()._id,
                    houseNumber: $scope.houseno,
                    street: $scope.street,
                    city: $scope.city,
                    postalCode: $scope.postalcode,
                    country: $scope.country
                })
                .success(function(response) {
                    $scope.address = response;      // store updated value
                    $rootScope.$broadcast('AddressCreated', $scope.address );
                    $mdDialog.hide(true);
                })
                .error(function(response) {
                    $mdDialog.hide(false);
                    showSimpleToast("Error. Please try again later");
                });
            };
        };
    }]
);
